package com.example.migue.appelo;


import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.MediaMetadataRetriever;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.preference.PreferenceManager;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.Html;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
//import android.util.Log;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
//import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;
import java.io.File;
//import java.io.Serializable;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import com.example.migue.appelo.DB.ConfigurationOperations;
import com.example.migue.appelo.Model.Configuration;
import static com.example.migue.appelo.Utils.getLocalIpAddress;
import static com.example.migue.appelo.Utils.getConfiguration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

public class WelcomeActivity extends AppCompatActivity   {
    private VideoView videoView;
    private ConfigurationOperations configurationData;
    public Configuration datos;
    //private Button buttonConn;
    public EditText editTextMSG,editTextMSGV;
    public TextView textViewWelcome,textViewVersion;
    public String globalCadena,nombreLocal,abrevLocal,miIp,picture = "";
    private ImageView logoWelcome;
    private int i=0;
    public Runnable r;
    //public Runnable ro;
    public List<String> mStringEjecucion = new ArrayList<String>();
    public List<String> mStringEjecucionCompare = new ArrayList<String>();
    public long tiempoPublicidad,bandera;
    public String[] strings;
    public Dialog publicityDialog;
    public Bitmap myBitmap;
    public Timer timer2, timer3;
    public CoordinatorLayout welcomeLayout;


    Handler handler=new Handler(){
        @Override
        public void handleMessage(Message msg) {
            if (msg.what==100){
                i=0;
            }
        }
    };

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        stopHandler();//stop first and then start
        if (mStringEjecucion.size() > 0 && datos != null) {
            if (datos.getMuestraPublicidad().intValue() == 1){
                startHandler();
            }

        }
    }
    public void stopHandler() {
        handler.removeCallbacks(r);
        //handler.removeCallbacks(ro);
        handler.removeCallbacksAndMessages(null);

    }
    public void startHandler() {

        if (datos != null) {
            handler.postDelayed(r, datos.getTiempoInactividad()); //for 5 minutes
        }

    }

    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        /* INICIALIZACION DE VIEWS */
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        //buttonConn =  findViewById(R.id.buttonConn);
        logoWelcome = findViewById(R.id.logoWelcome);
        textViewVersion = findViewById(R.id.textViewVersion);
        welcomeLayout =  findViewById(R.id.welcomeLayout);
        //ImageView logoWelcome = findViewById(R.id.logoWelcome);
        textViewWelcome =  findViewById(R.id.textViewWelcome);
        editTextMSG = findViewById(R.id.editTextMSG);

        Thread.setDefaultUncaughtExceptionHandler(new MyExceptionHandler(this));

        //editTextMSG.setText("4050955842923");
        //miIp = getLocalIpAddress(0).toString();

        /* OBTENGO DATOS DE IMAGENES Y VIDEOS PARA PUBLICIDAD CENCOSUD */
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            mStringEjecucion = bundle.getStringArrayList("mStringEjecucion");
            strings = bundle.getStringArray("strings");
            globalCadena = "init";
        }
        configurationData = getConfiguration(WelcomeActivity.this);
        datos = configurationData.getConfigurationByStatus(1);
        Global.datos = datos;


                if (datos != null) {
                    if (datos.getSkin().intValue() == 1) {
                        welcomeLayout.setBackgroundColor(Color.parseColor("#00AAEF"));
                        logoWelcome.setImageResource(R.drawable.logo_paris_cencosud);
                    } else if (datos.getSkin().intValue() == 2) {
                        welcomeLayout.setBackgroundColor(Color.parseColor("#FF6600"));
                        logoWelcome.setImageResource(R.drawable.logo_johnson_cencosud);
                    }
                    if (datos.getSkin().intValue() == 1 || datos.getSkin().intValue() == 2) {
                        textViewVersion.setTextColor(Color.parseColor("#FFFFFF"));
                        textViewWelcome.setTextColor(Color.parseColor("#FFFFFF"));
                        nombreLocal = datos.getNombreLocal();
                        abrevLocal = datos.getAbrevLocal();
                    }
                    textViewVersion.setText(Html.fromHtml(nombreLocal +"<br>Version 1.1 - " + abrevLocal + miIp));
                    if ( mStringEjecucion.size() > 0 && datos.getMuestraPublicidad().toString().equals("1")){
                        //Log.i("publicidad",String.valueOf(mStringEjecucion));
                        //startPublicity();
                    }

                } else {
                    textViewWelcome.setText("NO CONFIGURATION DATA");
                }



        editTextMSG.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() > 12) {
                    editTextMSG.setEnabled(false);
                    if (editable.equals("0001110009997")) {
                        stopHandler();
                        Intent i = new Intent(WelcomeActivity.this, retailActivity.class);
                        startActivity(i);
                    } else {
                        if (datos != null) {
                            conexionSocket(editTextMSG.getText().toString().substring(0,13));
                        }
                    }
                    editTextMSG.setText("");
                }
            }
        });


        /*
        findViewById(R.id.buttonConn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                conexionSocket(editTextMSG.getText().toString());
            }
        });
        */

        logoWelcome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (i==0){
                    ++i;
                    handler.sendEmptyMessageDelayed(100,2000); // 3000 equal 3sec , you can set your own limit of secs
                }else if(i==2){
                    //Toast.makeText(WelcomeActivity.this, "Three Touch Clicked" , Toast.LENGTH_SHORT).show();
                    i=0;
                    handler.removeMessages(100);

                    final AlertDialog.Builder alert = new AlertDialog.Builder(WelcomeActivity.this);
                    final EditText edittext = new EditText(WelcomeActivity.this);
                    edittext.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_VARIATION_PASSWORD);
                    edittext.setFilters(new InputFilter[] {new InputFilter.LengthFilter(10)});
                    alert.setMessage("");
                    alert.setTitle("Cencosud admin panel");

                    LinearLayout layout = new LinearLayout(WelcomeActivity.this);
                    layout.setOrientation(LinearLayout.VERTICAL);
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    params.setMargins(150, 0, 150, 0);
                    layout.addView(edittext, params);
                    alert.setView(layout);

                    alert.setPositiveButton("Enviar", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            String YouEditTextValue = edittext.getText().toString();
                            if (YouEditTextValue.equals("11111")) {
                                stopHandler();
                                Intent i = new Intent(WelcomeActivity.this, retailActivity.class);
                                startActivity(i);
                            } else {
                                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                                dialog.dismiss();
                            }
                        }
                    });

                    alert.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                        }
                    });

                    alert.show();
                }else
                    ++i;
            }
        });

/*
        ro = new Runnable() {
            public void run() {
                conexionSocket("1234567891234");
            }
        };
        handler.postDelayed(ro, 15000);
*/




    }

    private void setText(final TextView text,final String value){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                text.setText(value);
            }
        });
        editTextMSG.setEnabled(true);
    }

    private void conexionSocket(String texto){
        if (isNetworkAvailable() == true) {
            try {
                if (timer2!= null) {
                    timer2.cancel();
                    timer2=null;
                }
            } catch (Exception e) {

            }

            try {
                if (myBitmap != null) {
                    myBitmap.recycle();
                    myBitmap = null;
                }
            } catch (Exception e) {

            }

            /*
            if (videoView!= null) {
                videoView.stopPlayback();
            }
            if (myBitmap != null) {
                myBitmap.recycle();
                myBitmap = null;
            }
            */
            editTextMSG.setEnabled(true);
            editTextMSG.setText("");
            stopHandler();
            Log.i("info","conexion");

            Intent i = new Intent(WelcomeActivity.this, MainActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString("codeMSG", texto);
            i.putExtras(bundle);

            overridePendingTransition(0, 0);
            startActivity(i);
            //System.exit(0);

        } else {
            textViewWelcome.setText("Sin conexion de red");
            timer3= new Timer();
            timer3.schedule(new TimerTask() {
                public void run() {
                    setText(textViewWelcome,"Acerca tu producto para conocer su precio");
                    setText(editTextMSG,"");
                    //textViewWelcome.setText("Acerca tu producto para conocer su precio");
                    timer3.cancel();
                }
            }, 2000);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Runtime.getRuntime().gc();
        System.exit(0);
    }

    private void startPublicity(){

        r = new Runnable() {
            @Override
            public void run() {
                picture = "";
                publicityDialog = new Dialog(WelcomeActivity.this);
                publicityDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                publicityDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

                publicityDialog.setOnCancelListener(
                        new DialogInterface.OnCancelListener() {
                            @Override
                            public void onCancel(DialogInterface dialog) {
                                if (timer2!= null) {
                                    timer2.cancel();
                                    //timer2=null;
                                }
                                try {

                                    videoView.stopPlayback();
                                } catch (Exception e) {

                                }
                                try {
                                    myBitmap.recycle();
                                    myBitmap = null;
                                } catch (Exception e) {

                                }
                                try {

                                    stopHandler();
                                    globalCadena = "init";
                                    mStringEjecucionCompare.clear();
                                    startHandler();
                                } catch (Exception e) {

                                }
                            }
                        }
                );

                if (datos.getSkin().intValue() == 2){
                    publicityDialog.getWindow().setBackgroundDrawableResource(R.color.colorOrangeCencosud);
                } else {
                    publicityDialog.getWindow().setBackgroundDrawableResource(R.color.colorBlueCensosud);
                }
                if (globalCadena.equals("init")) {
                    mStringEjecucionCompare.clear();
                    mStringEjecucionCompare.add(strings[0]);
                    globalCadena = "init0";
                    picture = strings[0];
                } else {
                    for (int i = 0; i < strings.length; i++) {
                        Boolean bool = mStringEjecucionCompare.contains(strings[i]) ? true : false;
                        if (mStringEjecucionCompare.size() > datos.getCantImagenes()) {
                            globalCadena = "init";
                            break;
                        }
                        if (mStringEjecucion.size() == mStringEjecucionCompare.size()) {
                            globalCadena = "init";
                            break;
                        }
                        if (bool == false) {
                            mStringEjecucionCompare.add(strings[i]);
                            picture = strings[i];
                            break;
                        }
                    }
                }


                File miFile = new File(picture);
                if (miFile.exists()) {

                    if (picture.contains(".mp4")) {
                            MediaMetadataRetriever retriever = new MediaMetadataRetriever();
                            retriever.setDataSource(getApplicationContext(), Uri.fromFile(miFile));
                            String time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
                            long timeInMillisec = Long.parseLong(time);
                            tiempoPublicidad = timeInMillisec;
                            //Log.i("Tiempo publicidad",String.valueOf(tiempoPublicidad));
                            retriever.release();

                            publicityDialog.setContentView(getLayoutInflater().inflate(R.layout.video_layout
                                    , null));
                            videoView = publicityDialog.findViewById(R.id.videoView);

                            Uri uri = Uri.parse(picture);
                            videoView.setVideoURI(uri);
                            videoView.setZOrderOnTop(true);

                            MediaController mc = new MediaController(WelcomeActivity.this);
                            videoView.setMediaController(mc);
                            videoView.start();
                    } else {
                        tiempoPublicidad = datos.getTiempoImagen();
                        publicityDialog.setContentView(getLayoutInflater().inflate(R.layout.image_layout
                                , null));
                        ImageView image = publicityDialog.findViewById(R.id.image_dialog);

                        BitmapFactory.Options options = new BitmapFactory.Options();
                        options.inSampleSize = 1;

                        myBitmap = BitmapFactory.decodeFile(miFile.getAbsolutePath(),options);
                        image.setImageBitmap(myBitmap);
                    }
                } else {
                    publicityDialog.dismiss();
                    handler.postDelayed(r, 100);
                }


                if (picture.contains(".mp4") || picture.contains(".jpg") || picture.contains(".png") || picture.contains(".bmp")) {
                    editTextMSGV = publicityDialog.findViewById(R.id.editTextMSGV);

                    TextView textBotton = publicityDialog.findViewById(R.id.textBotton);
                    textBotton.setText("Acerca tu producto para conocer su precio");

                    if (picture.contains(".mp4")) {
                        videoView.setOnTouchListener(new View.OnTouchListener() {
                            @Override
                            public boolean onTouch(View view, MotionEvent motionEvent) {
                                return false;
                            }
                        });
                    }


                    editTextMSGV.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                        }

                        @Override
                        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                        }

                        @Override
                        public void afterTextChanged(Editable editable) {
                            if (editable.length() > 12) {
                                bandera = 0;
                                try {
                                    bandera = Long.parseLong(editable.toString());
                                    editTextMSGV.setEnabled(false);
                                    if (datos != null) {
                                            conexionSocket(editable.toString().substring(0,13));
                                    } else {
                                        editTextMSGV.setText("");
                                    }
                                } catch (NumberFormatException e) {
                                    if (editable.equals("CEN-cen-43654")) {
                                        stopHandler();
                                        Intent i = new Intent(WelcomeActivity.this, retailActivity.class);
                                        startActivity(i);
                                    }
                                    editTextMSGV.setText("");
                                }
                            }
                        }
                    });
                    //editTextMSGV.requestFocus();




                    publicityDialog.show();
                    editTextMSGV.requestFocus();

                    timer2= new Timer();
                    timer2.schedule(new TimerTask() {
                        public void run() {
                            publicityDialog.dismiss();
                            /*
                            if (videoView!= null) {
                                videoView.stopPlayback();
                            }
                            */
                            if (myBitmap != null) {
                                myBitmap.recycle();
                                myBitmap = null;
                            }

                            timer2.cancel();
                        }
                    }, tiempoPublicidad - 100);
                    handler.postDelayed(r, tiempoPublicidad);
                    //editTextMSGV.requestFocus();
                }
            }
        };
        startHandler();
    }
}

