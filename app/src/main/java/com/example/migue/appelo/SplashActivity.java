package com.example.migue.appelo;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.content.Intent;

import android.support.annotation.Nullable;
//import android.util.Log;
import android.util.Pair;
import android.widget.Toast;

import com.example.migue.appelo.DB.ConfigurationOperations;
import com.example.migue.appelo.DB.LogOperations;
import com.example.migue.appelo.Model.Configuration;
import com.example.migue.appelo.Model.Logs;

import java.io.File;
import java.io.Serializable;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import static com.example.migue.appelo.Utils.getConfiguration;
import static com.example.migue.appelo.Utils.imagenesYvideos;



public class SplashActivity extends AppCompatActivity implements Serializable {
    private List<String> mStringEjecucion = new ArrayList<String>();
    private String[] strings;
    private static Logs newLogs;
    private static LogOperations logData;
    private ConfigurationOperations configurationData;
    public Configuration datos;
    private AlarmManager alarmMgr;
    private PendingIntent alarmIntent;
    private PendingIntent alarmIntent2;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        setCustomDatabaseFiles(getApplicationContext());
        try {
            Class<?> debugDB = Class.forName("com.example.migue.appelo.DebugDB");
            Method getAddressLog = debugDB.getMethod("getAddressLog");
            Object value = getAddressLog.invoke(null);
            //Toast.makeText(getApplicationContext(), (String) value, Toast.LENGTH_LONG).show();
        } catch (Exception ignore) {

        }

        configurationData = getConfiguration(this);
        datos = configurationData.getConfigurationByStatus(1);
        Global.datos = datos;
        if (datos != null) {
            SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.DATE, -datos.getLogNroDias());
            newLogs = new Logs();
            logData = new LogOperations(SplashActivity.this);
            logData.open();
            logData.removeLogs(s.format(new Date(cal.getTimeInMillis())));
        }

        Runnable r = new Runnable() {
            @Override
            public void run() {
                /* SEARCH ROUTES OF IMAGES AN VIDEOS */
                mStringEjecucion = imagenesYvideos(getApplicationContext());
                strings = new String[mStringEjecucion.size()];
                strings = mStringEjecucion.toArray(strings);
                //stringsCompare = new String[mStringEjecucion.size()];

                Global.mStringEjecucion = mStringEjecucion;
                Global.strings = strings;

                /*Bundle bundle = new Bundle();
                bundle.putStringArrayList("mStringEjecucion",(ArrayList<String>)mStringEjecucion);
                bundle.putStringArray("strings", strings);
                */
                Intent i = new Intent(SplashActivity.this, MainActivity.class);


                //i.putExtras(bundle);
                startActivity(i);
                finish();
            }
        };

        Handler h = new Handler();
        h.postDelayed(r, 2000);



    }

    public static void setCustomDatabaseFiles(Context context) {

        try {
            Class<?> debugDB = Class.forName("com.example.migue.appelo.DebugDB");
            Class[] argTypes = new Class[]{HashMap.class};
            Method setCustomDatabaseFiles = debugDB.getMethod("setCustomDatabaseFiles", argTypes);
            HashMap<String, Pair<File, String>> customDatabaseFiles = new HashMap<>();

        } catch (Exception ignore) {

        }

    }
}
