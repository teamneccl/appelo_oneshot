package com.example.migue.appelo;

import android.content.Context;
import android.database.Cursor;
import android.provider.MediaStore;
import android.util.Log;
import android.util.Pair;

import com.example.migue.appelo.DB.ConfigurationOperations;
//import com.example.migue.appelo.DB.LogOperations;
import com.example.migue.appelo.DB.LogOperations;
import com.example.migue.appelo.Model.Configuration;
import com.example.migue.appelo.Model.Logs;
//import com.example.migue.appelo.Model.Logs;

import java.io.File;
import java.lang.reflect.Method;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;

/**
 * Created by migue on 25/12/2017.
 */

public class Utils {
    private static Configuration newConfiguration;
    private static ConfigurationOperations configurationData;
    private static Logs newLogs;
    private static LogOperations logData;
    private static List<String> mStringsPromociones = new ArrayList<String>();
    private static List<String> mStringsNormales = new ArrayList<String>();
    private static List<String> mStringEjecucion = new ArrayList<String>();


    public static List<String> imagenesYvideos(Context context){
        mStringEjecucion.clear();
        mStringsPromociones.clear();
        mStringsNormales.clear();

        Cursor cursorVideo = context.getContentResolver().query(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, new String[]{MediaStore.Files.FileColumns.DATA}, null, null, null + " DESC");
        while (cursorVideo.moveToNext()) {
            String hiddenFilePathV = cursorVideo.getString(cursorVideo
                    .getColumnIndex(MediaStore.Files.FileColumns.DATA));
            //Log.i("video file", String.valueOf(hiddenFilePathV));
            if (String.valueOf(hiddenFilePathV).toLowerCase().indexOf("promociones") != -1) {
                mStringsPromociones.add(String.valueOf(hiddenFilePathV));
            }
            if (String.valueOf(hiddenFilePathV).toLowerCase().indexOf("normales") != -1) {
                mStringsNormales.add(String.valueOf(hiddenFilePathV));
            }
        }
        cursorVideo.close();


        Cursor cursor = context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, new String[]{MediaStore.Files.FileColumns.DATA}, null, null, null + " DESC");
        while (cursor.moveToNext()) {
            String hiddenFilePath = cursor.getString(cursor
                    .getColumnIndex(MediaStore.Files.FileColumns.DATA));
            //Log.i("image file", String.valueOf(hiddenFilePath));

            if (String.valueOf(hiddenFilePath).toLowerCase().indexOf("promociones") != -1) {
                mStringsPromociones.add(String.valueOf(hiddenFilePath));
            }
            if (String.valueOf(hiddenFilePath).toLowerCase().indexOf("normales") != -1) {
                mStringsNormales.add(String.valueOf(hiddenFilePath));
            }
        }
        cursor.close();
        mStringEjecucion = mStringsNormales;
        if (mStringsPromociones.size() > 0) {
            mStringEjecucion = mStringsPromociones;
        }
        return mStringEjecucion;
    }

    public static ConfigurationOperations getConfiguration(Context context){
        newConfiguration = new Configuration();
        configurationData = new ConfigurationOperations(context);
        configurationData.open();
        return configurationData;
    }

    public static void LogRegister(Context context, String tipo, String inputDesc, String outputDesc){
        newLogs = new Logs();
        logData = new LogOperations(context);
        logData.open();
        String currentDateandTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
        newLogs.setLogDate(currentDateandTime);
        newLogs.setInputDesc(inputDesc);
        newLogs.setOutputDesc(outputDesc);
        newLogs.setLogType(tipo);
        logData.addLog(newLogs);
    }

    public static String xmlEnvelop(String codemsg, String local){
        String xml="<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:onl=\"http://www.cencosud.corp/DSCL_OmniChannel_INT2336/OnlineStockEBM\" xmlns:ebm=\"http://xmlns.cencosud.corp/Core/EBM/Common/EBM\" xmlns:onl1=\"http://www.cencosud.corp/DSCL_OmniChannel_INT2336/OnlineStockInputMessage\">\n" +
                "   <soapenv:Header/>\n" +
                "   <soapenv:Body>\n" +
                "      <onl:OnlineStockRequestEBM>\n" +
                "         <ebm:EBMHeader>\n" +
                "            <ebm:EBMID>?</ebm:EBMID>\n" +
                "            <!--Optional:-->\n" +
                "            <ebm:CreationDateTime>?</ebm:CreationDateTime>\n" +
                "            <ebm:Sender>\n" +
                "               <ebm:Application>?</ebm:Application>\n" +
                "               <ebm:Country>?</ebm:Country>\n" +
                "               <ebm:BusinessUnit>?</ebm:BusinessUnit>\n" +
                "               <!--Optional:-->\n" +
                "               <ebm:LegalEntity>?</ebm:LegalEntity>\n" +
                "            </ebm:Sender>\n" +
                "            <!--Optional:-->\n" +
                "            <ebm:Target>?</ebm:Target>\n" +
                "            <!--Zero or more repetitions:-->\n" +
                "            <ebm:EBMTracking>\n" +
                "               <!--Optional:-->\n" +
                "               <ebm:FileName>?</ebm:FileName>\n" +
                "               <!--Optional:-->\n" +
                "               <ebm:ParentEBMID>?</ebm:ParentEBMID>\n" +
                "               <!--Optional:-->\n" +
                "               <ebm:IntegrationCode>?</ebm:IntegrationCode>\n" +
                "               <!--Optional:-->\n" +
                "               <ebm:ReferenceID>?</ebm:ReferenceID>\n" +
                "            </ebm:EBMTracking>\n" +
                "            <!--Zero or more repetitions:-->\n" +
                "            <ebm:Addressing>\n" +
                "               <!--Optional:-->\n" +
                "               <ebm:ReplyToAddress>?</ebm:ReplyToAddress>\n" +
                "               <!--Optional:-->\n" +
                "               <ebm:CorrelID>?</ebm:CorrelID>\n" +
                "            </ebm:Addressing>\n" +
                "         </ebm:EBMHeader>\n" +
                "         <DataArea>\n" +
                "            <onl1:stock_online_req>\n" +
                "               <local_consulta>"+local.toString()+"</local_consulta>\n" +
                "               <lector>\"10.95.61.177\"</lector>\n" +
                "               <sku_ean>"+codemsg.toString()+"</sku_ean>\n" +
                "            </onl1:stock_online_req>\n" +
                "         </DataArea>\n" +
                "      </onl:OnlineStockRequestEBM>\n" +
                "   </soapenv:Body>\n" +
                "</soapenv:Envelope>";
        return xml;
    }


    public static String getLocalIpAddress(Integer opcion)
    {
        //StringBuilder IFCONFIG=new StringBuilder();
        String[] arrayIP = null;
        String returnIP = "000.00.00.00";
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements();) {
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements();) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress() && !inetAddress.isLinkLocalAddress() && inetAddress.isSiteLocalAddress()) {
                        if (opcion == 1) {
                            return (inetAddress.getHostAddress().toString()+"\n");
                        }
                        returnIP = (inetAddress.getHostAddress().toString()+"\n");
                        arrayIP = returnIP.split("[.]");
                    }
                }
            }
        } catch (Exception ex) {
            if (opcion == 1) {
                return returnIP;
            }
            arrayIP = returnIP.split("[.]");
            return arrayIP[3].toString();
        }
        return arrayIP[3].toString();
        //return "00";
    }

    public static String getXMLStringONE(){
        String xmlString = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "<response>\n" +
                "               <cod_resp>00</cod_resp>\n" +
                "               <msg_resp>ENCONTRADO</msg_resp>\n" +
                "               <desc_art>SAND AZALEI NAHIR/FL</desc_art>\n" +
                "               <desc_local>ALAMEDA</desc_local>\n" +
                "               <sku>196642009</sku>\n" +
                "               <ean>2019664200983</ean>\n" +
                "               <stock>\n" +
                "                  <sku>196642002</sku>\n" +
                "                  <stock_ant>22</stock_ant>\n" +
                "                  <ventas/>\n" +
                "                  <nota_credito/>\n" +
                "                  <stock_act>22</stock_act>\n" +
                "                  <mes_antiguedad>000</mes_antiguedad>\n" +
                "                  <talla>test de cualquier cosa</talla>\n" +
                "                  <color>Morado Oscuro 0</color>\n" +
                "               </stock>\n" +
                "</response>\n";
        return xmlString;
    }

    public static String getXMLStringCERO(){
        String xmlString = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "<response>\n" +
                "               <cod_resp>00</cod_resp>\n" +
                "               <msg_resp>ENCONTRADO</msg_resp>\n" +
                "               <desc_art>SAND AZALEI NAHIR/FL</desc_art>\n" +
                "               <desc_local>ALAMEDA</desc_local>\n" +
                "               <sku>196642009</sku>\n" +
                "               <ean>2019664200983</ean>\n" +

                "</response>\n";
        return xmlString;
    }


    public static String getXMLString(){
        String xmlString = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "<response>\n" +
                "               <cod_resp>00</cod_resp>\n" +
                "               <msg_resp>ENCONTRADO</msg_resp>\n" +
                "               <desc_art>SAND AZALEI NAHIR/FL</desc_art>\n" +
                "               <desc_local>ALAMEDA</desc_local>\n" +
                "               <sku>196642009</sku>\n" +
                "               <ean>2019664200983</ean>\n" +
                "               <stock>\n" +
                "                  <sku>196642002</sku>\n" +
                "                  <stock_ant>22</stock_ant>\n" +
                "                  <ventas/>\n" +
                "                  <nota_credito/>\n" +
                "                  <stock_act>-2</stock_act>\n" +
                "                  <mes_antiguedad>000</mes_antiguedad>\n" +
                "                  <talla>N°35</talla>\n" +
                "                  <color>Morado Oscuro 0</color>\n" +
                "               </stock>\n" +
                "               <stock>\n" +

                "                  <sku>196642003</sku>\n" +
                "                  <stock_ant>24</stock_ant>\n" +
                "                  <ventas/>\n" +
                "                  <nota_credito/>\n" +
                "                  <stock_act>24</stock_act>\n" +
                "                  <mes_antiguedad>000</mes_antiguedad>\n" +
                "                  <talla>N°36</talla>\n" +
                "                  <color>Morado Oscuro 1</color>\n" +
                "               </stock>\n" +
                "               <stock>\n" +

                "                  <sku>196642004</sku>\n" +
                "                  <stock_ant>25</stock_ant>\n" +
                "                  <ventas/>\n" +
                "                  <nota_credito/>\n" +
                "                  <stock_act>25</stock_act>\n" +
                "                  <mes_antiguedad>000</mes_antiguedad>\n" +
                "                  <talla>N°37</talla>\n" +
                "                  <color>Morado Oscuro 2</color>\n" +
                "               </stock>\n" +
                "               <stock>\n" +

                "                  <sku>196642005</sku>\n" +
                "                  <stock_ant>24</stock_ant>\n" +
                "                  <ventas/>\n" +
                "                  <nota_credito/>\n" +
                "                  <stock_act>24</stock_act>\n" +
                "                  <mes_antiguedad>000</mes_antiguedad>\n" +
                "                  <talla>N°38</talla>\n" +
                "                  <color>Morado Oscuro 3</color>\n" +
                "               </stock>\n" +
                "               <stock>\n" +

                "                  <sku>196642006</sku>\n" +
                "                  <stock_ant>24</stock_ant>\n" +
                "                  <ventas/>\n" +
                "                  <nota_credito/>\n" +
                "                  <stock_act>24</stock_act>\n" +
                "                  <mes_antiguedad>000</mes_antiguedad>\n" +
                "                  <talla>N°39</talla>\n" +
                "                  <color>Morado Oscuro 4</color>\n" +
                "               </stock>\n" +
                "               <stock>\n" +

                "                  <sku>196642007</sku>\n" +
                "                  <stock_ant>23</stock_ant>\n" +
                "                  <ventas/>\n" +
                "                  <nota_credito/>\n" +
                "                  <stock_act>23</stock_act>\n" +
                "                  <mes_antiguedad>000</mes_antiguedad>\n" +
                "                  <talla>N°40</talla>\n" +
                "                  <color>Morado Oscuro 5</color>\n" +
                "               </stock>\n" +
                "               <stock>\n" +

                "                  <sku>196642008</sku>\n" +
                "                  <stock_ant>23</stock_ant>\n" +
                "                  <ventas/>\n" +
                "                  <nota_credito/>\n" +
                "                  <stock_act>0</stock_act>\n" +
                "                  <mes_antiguedad>000</mes_antiguedad>\n" +
                "                  <talla>N°35</talla>\n" +
                "                  <color>Morado Oscuro 6</color>\n" +
                "               </stock>\n" +
                "               <stock>\n" +

                "                  <sku>196642009</sku>\n" +
                "                  <stock_ant>23</stock_ant>\n" +
                "                  <ventas/>\n" +
                "                  <nota_credito/>\n" +
                "                  <stock_act>23</stock_act>\n" +
                "                  <mes_antiguedad>000</mes_antiguedad>\n" +
                "                  <talla>N°36</talla>\n" +
                "                  <color>Morado Oscuro 7</color>\n" +
                "               </stock>\n" +
                "               <stock>\n" +

                "                  <sku>196642010</sku>\n" +
                "                  <stock_ant>22</stock_ant>\n" +
                "                  <ventas/>\n" +
                "                  <nota_credito/>\n" +
                "                  <stock_act>22</stock_act>\n" +
                "                  <mes_antiguedad>000</mes_antiguedad>\n" +
                "                  <talla>N°37</talla>\n" +
                "                  <color>Morado Oscuro 8</color>\n" +
                "               </stock>\n" +
                "               <stock>\n" +

                "                  <sku>196642016</sku>\n" +
                "                  <stock_ant>22</stock_ant>\n" +
                "                  <ventas/>\n" +
                "                  <nota_credito/>\n" +
                "                  <stock_act>22</stock_act>\n" +
                "                  <mes_antiguedad>000</mes_antiguedad>\n" +
                "                  <talla>N°37</talla>\n" +
                "                  <color>Morado Oscuro 18</color>\n" +
                "               </stock>\n" +
                "               <stock>\n" +

                "                  <sku>196642011</sku>\n" +
                "                  <stock_ant>25</stock_ant>\n" +
                "                  <ventas/>\n" +
                "                  <nota_credito/>\n" +
                "                  <stock_act>25</stock_act>\n" +
                "                  <mes_antiguedad>000</mes_antiguedad>\n" +
                "                  <talla>N°38</talla>\n" +
                "                  <color>Morado Oscuro 9</color>\n" +
                "               </stock>\n" +
                "               <stock>\n" +

                "                  <sku>196642012</sku>\n" +
                "                  <stock_ant>24</stock_ant>\n" +
                "                  <ventas/>\n" +
                "                  <nota_credito/>\n" +
                "                  <stock_act>24</stock_act>\n" +
                "                  <mes_antiguedad>000</mes_antiguedad>\n" +
                "                  <talla>N°39</talla>\n" +
                "                  <color>Morado Oscuro 10</color>\n" +
                "               </stock>\n" +
                "               <stock>\n" +
                "                  <sku>196642013</sku>\n" +
                "                  <stock_ant>23</stock_ant>\n" +
                "                  <ventas/>\n" +
                "                  <nota_credito/>\n" +
                "                  <stock_act>23</stock_act>\n" +
                "                  <mes_antiguedad>000</mes_antiguedad>\n" +
                "                  <talla>N°41</talla>\n" +
                "                  <color>Morado Oscuro 11</color>\n" +
                "               </stock>\n" +
                "               <stock>\n" +
                "                  <sku>196642053</sku>\n" +
                "                  <stock_ant>23</stock_ant>\n" +
                "                  <ventas/>\n" +
                "                  <nota_credito/>\n" +
                "                  <stock_act>23</stock_act>\n" +
                "                  <mes_antiguedad>000</mes_antiguedad>\n" +
                "                  <talla>N°41/C</talla>\n" +
                "                  <color>Morado claro 11</color>\n" +
                "               </stock>\n" +
                "</response>\n";
        return xmlString;
    }
}
