package com.example.migue.appelo.Server;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by macevedo on 11-01-2018.
 */

public class Session {

    private SharedPreferences prefs;

    public Session(Context cntx) {
        // TODO Auto-generated constructor stub
        prefs = PreferenceManager.getDefaultSharedPreferences(cntx);
    }

    public void setSession(String usename) {
        prefs.edit().putString("usename", usename).commit();
    }

    public String getSession() {
        String usename = prefs.getString("usename","");
        return usename;
    }

    public void cleanSession(){

        prefs.edit().remove("usename").commit();
        //prefs.edit().clear().commit();
        //prefs.edit().apply();
        //prefs.edit().commit();
    }
}