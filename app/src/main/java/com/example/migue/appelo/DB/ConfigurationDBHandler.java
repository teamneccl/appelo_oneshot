package com.example.migue.appelo.DB;

import android.content.Context;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by migue on 16/12/2017.
 */

public class ConfigurationDBHandler extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "configuration.db";
    private static final int DATABASE_VERSION = 33;
    //29
    public static final String TABLE_CONFIGURATION = "configuration";
    // configuracion settings
    public static final String COLUMN_ID = "empId";
    public static final String COLUMN_IPDESC = "ipdesc";
    public static final String COLUMN_PORTDESC = "portdesc";
    public static final String COLUMN_TIPO = "tipo";
    public static final String COLUMN_LOCAL = "local";
    public static final String COLUMN_CONSULTOR = "consultor";
    public static final String COLUMN_NOMBRELOCAL = "nombrelocal";
    public static final String COLUMN_URLWS = "urlws";
    public static final String COLUMN_TPRECIOPANTALLA = "tpreciopantalla";
    public static final String COLUMN_SKIN = "skin";
    public static final String COLUMN_CONSULTASTOCK = "consultastock";
    public static final String COLUMN_STOCKMINIMO = "stockminimo";
    public static final String COLUMN_STATUS = "status";
    public static final String COLUMN_ABREVLOCAL = "abrevlocal";
    public static final String COLUMN_TIMESOCKET = "timesocket";
    public static final String COLUMN_TIMEOUTURLWS = "timeoutWS";
    public static final String COLUMN_LOGACTIVO = "logActivo";
    public static final String COLUMN_LOGNRODIAS = "logNroDias";
    public static final String COLUMN_STOCKGRILLA = "stockGrilla";
    public static final String COLUMN_HOUR = "rebootHour";
    public static final String COLUMN_MINUTE = "rebootMinute";
    public static final String COLUMN_VOZPRECIO = "vozPrecio";
    public static final String COLUMN_PASSWORD = "contrasenaAdmin";

    // publicidad
    public static final String COLUMN_MUESTRAPUBLICIDAD = "muestraPublicidad";
    public static final String COLUMN_CANTIMAGENES = "cantidadImagenes";
    public static final String COLUMN_TIEMPOIMAGEN = "tiempoImagen";
    public static final String COLUMN_TIEMPOINACTIVIDAD = "tiempoInactividad";

    public static final String TABLE_LOGS = "logs";
    public static final String COLUMN_IDL = "logId";
    public static final String COLUMN_LOGTYPE = "logtype";
    public static final String COLUMN_INPUT = "inputdesc";
    public static final String COLUMN_OUTPUT = "outputdesc";
    public static final String COLUMN_DATE = "logdate";

    private static final String TABLE_CREATE_LOGS =
            "CREATE TABLE " + TABLE_LOGS + " (" +
                    COLUMN_IDL + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    COLUMN_LOGTYPE + " TEXT, " +
                    COLUMN_DATE + " TEXT , " +
                    COLUMN_INPUT + " BLOB, " +
                    COLUMN_OUTPUT + " BLOB ) ";

    private static final String TABLE_CREATE =
            "CREATE TABLE " + TABLE_CONFIGURATION + " (" +
                    COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    COLUMN_IPDESC + " TEXT, " +
                    COLUMN_PORTDESC + " TEXT, " +
                    COLUMN_TIPO + " TEXT, " +
                    COLUMN_LOCAL + " TEXT DEFAULT 0 , " +
                    COLUMN_CONSULTOR + " INTEGER DEFAULT 0 , " +
                    COLUMN_NOMBRELOCAL + " TEXT, " +
                    COLUMN_URLWS + " TEXT, " +
                    COLUMN_TPRECIOPANTALLA + " INTEGER DEFAULT 6000, " +
                    COLUMN_SKIN + " INTEGER DEFAULT 0 , " +
                    COLUMN_CONSULTASTOCK + " INTEGER DEFAULT 0 , " +
                    COLUMN_STOCKMINIMO + " INTEGER DEFAULT 0 , " +
                    COLUMN_STATUS + " INTEGER DEFAULT 0  ," +
                    COLUMN_ABREVLOCAL + " TEXT," +
                    COLUMN_MUESTRAPUBLICIDAD + " INTEGER DEFAULT 0 , " +
                    COLUMN_CANTIMAGENES + " INTEGER DEFAULT 0 , " +
                    COLUMN_TIEMPOIMAGEN + " INTEGER DEFAULT 0  , " +
                    COLUMN_TIEMPOINACTIVIDAD + " INTEGER DEFAULT 0, " +
                    COLUMN_TIMESOCKET + " INTEGER DEFAULT 0, " +
                    COLUMN_TIMEOUTURLWS + " INTEGER DEFAULT 0, " +
                    COLUMN_LOGACTIVO + " INTEGER DEFAULT 0  , " +
                    COLUMN_LOGNRODIAS + " INTEGER DEFAULT 30 , " +
                    COLUMN_STOCKGRILLA + " INTEGER DEFAULT 0 , " +
                    COLUMN_HOUR + " INTEGER DEFAULT 10 , " +
                    COLUMN_MINUTE + " INTEGER DEFAULT 0 , " +
                    COLUMN_VOZPRECIO + " INTEGER DEFAULT 0 , " +
                    COLUMN_PASSWORD + " INTEGER DEFAULT 0 " +
                    " ) ";


    public ConfigurationDBHandler(Context context){
        super(context,DATABASE_NAME,null,DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(TABLE_CREATE);
        //final String Insert_Data_init=" INSERT INTO " + TABLE_CONFIGURATION + " VALUES (null,'192.168.1.81','15001','RETAIL','00004',134,'CENCOSUD','http://g100603sv23k.cencosud.corp:7010/DSCL_ReetiquetadoTienda_INT2336/GetOnlineStock?WSDL',8000,2,1,3,1,'CENCVFPR',0,5,6000,20000,3000,2000,1,30,0,10,0,1);";
        //final String Insert_Data_init=" INSERT INTO " + TABLE_CONFIGURATION + " VALUES (null,'10.95.60.162','10105','RETAIL','00004',134,'CENCOSUD','http://g100603sv23k.cencosud.corp:7010/DSCL_ReetiquetadoTienda_INT2336/GetOnlineStock?WSDL',8000,2,1,3,1,'CENCVFPR',0,5,6000,20000,3000,2000,1,30,0,10,0,1);";
        //final String Insert_Data_init=" INSERT INTO " + TABLE_CONFIGURATION + " VALUES (null,'10.95.61.98','10105','RETAIL','00004',134,'CENCOSUD','http://g100603sv23k.cencosud.corp:7010/DSCL_ReetiquetadoTienda_INT2336/GetOnlineStock?WSDL',8000,2,1,3,1,'CENCVFPR',0,5,6000,20000,3000,2000,1,30,0,10,0,1,11111);";
        final String Insert_Data_init=" INSERT INTO " + TABLE_CONFIGURATION + " VALUES (null,'10.106.59.251','10105','RETAIL','00004',134,'CENCOSUD','http://g100603sv23k.cencosud.corp:7010/DSCL_ReetiquetadoTienda_INT2336/GetOnlineStock?WSDL',8000,1,1,3,1,'CENCVFPR',0,5,6000,20000,3000,2000,1,30,0,10,0,1,11111);";
        //Log.i("insert",Insert_Data_init);
        db.execSQL(Insert_Data_init);
        db.execSQL(TABLE_CREATE_LOGS);
        // first records

}

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS "+TABLE_CONFIGURATION);
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_LOGS);
        db.execSQL(TABLE_CREATE);
        db.execSQL(TABLE_CREATE_LOGS);
    }
    public ArrayList<Cursor> getData(String Query){
        //get writable database
        SQLiteDatabase sqlDB = this.getWritableDatabase();
        String[] columns = new String[] { "message" };
        //an array list of cursor to save two cursors one has results from the query
        //other cursor stores error message if any errors are triggered
        ArrayList<Cursor> alc = new ArrayList<Cursor>(2);
        MatrixCursor Cursor2= new MatrixCursor(columns);
        alc.add(null);
        alc.add(null);

        try{
            String maxQuery = Query ;
            //execute the query results will be save in Cursor c
            Cursor c = sqlDB.rawQuery(maxQuery, null);

            //add value to cursor2
            Cursor2.addRow(new Object[] { "Success" });

            alc.set(1,Cursor2);
            if (null != c && c.getCount() > 0) {

                alc.set(0,c);
                c.moveToFirst();
                c.close();
                return alc ;
            }
            c.close();
            return alc;

        } catch(Exception ex){
            Log.d("printing exception", ex.getMessage());

            //if any exceptions are triggered save the error message to cursor an return the arraylist
            Cursor2.addRow(new Object[] { ""+ex.getMessage() });
            alc.set(1,Cursor2);
            return alc;
        }
    }
}