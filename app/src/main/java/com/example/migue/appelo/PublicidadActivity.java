package com.example.migue.appelo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.InputFilter;
import android.text.Spanned;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.migue.appelo.DB.ConfigurationOperations;
import com.example.migue.appelo.Model.Configuration;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static com.example.migue.appelo.Utils.getConfiguration;
import static com.example.migue.appelo.Utils.imagenesYvideos;

public class PublicidadActivity extends AppCompatActivity
 {
     private Long theId;
     private Button buttonSettings,buttonHome,buttonSAVE;
     private Configuration newConfiguration;
     //private ConfigurationOperations configurationData;
     private ConfigurationOperations configurationData;
     private CheckBox checkbox,checkboxLoog,checkboxGrilla,checkboxVoz;
     private EditText cantidadImagenes,tiempoImagenes,TiempoInactividad,contrasenaAdmin;
     private Spinner spinner;

     public Configuration datos;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_publicidad);
        buttonSettings = findViewById(R.id.buttonSettings);
        buttonHome = findViewById(R.id.buttonHome);
        buttonSAVE =  findViewById(R.id.buttonSAVE);
        checkbox =  findViewById(R.id.checkbox);
        checkboxLoog =  findViewById(R.id.checkboxLoog);
        checkboxGrilla = findViewById(R.id.checkboxGrilla);
        checkboxVoz = findViewById(R.id.checkboxVoz);
        cantidadImagenes = findViewById(R.id.cantidadImagenes);
        tiempoImagenes = findViewById(R.id.tiempoImagenes);
        TiempoInactividad = findViewById(R.id.TiempoInactividad);
        contrasenaAdmin = findViewById(R.id.contrasenaAdmin);
        spinner = findViewById(R.id.spinner);
        //dlgDateTimePickerTime = findViewById(R.id.dlgDateTimePickerTime);


        newConfiguration = new Configuration();
        configurationData = new ConfigurationOperations(this);
        configurationData.open();

        Configuration datos = configurationData.getConfiguration("RETAIL");
        Global.datos = datos;

        if (datos != null) {
            theId = Long.valueOf(datos.getEmpId());
            if (datos.getMuestraPublicidad() == 1){
                checkbox.setChecked(true);
            }
            cantidadImagenes.setText(datos.getCantImagenes().toString());
            Log.i("cant",datos.getCantImagenes().toString());
            tiempoImagenes.setText(datos.getTiempoImagen().toString());
            TiempoInactividad.setText(datos.getTiempoInactividad().toString());
            if (datos.getContrasenaAdmin().equals("")) {
                contrasenaAdmin.setText("11111");
            } else {
                contrasenaAdmin.setText(datos.getContrasenaAdmin().toString());
            }


            if (datos.getLogActivo() == 1){
                checkboxLoog.setChecked(true);
            }

            if (datos.getVozPrecio() == 1){
                checkboxVoz.setChecked(true);
            }

            if (datos.getStockGrilla() == 1){
                checkboxGrilla.setChecked(true);
            }

            ArrayAdapter<String> array_spinner=(ArrayAdapter<String>)spinner.getAdapter();
            if (datos.getStatus() == 30) {
                spinner.setSelection(array_spinner.getPosition("30 días"));
            }
            if (datos.getStatus() == 15) {
                spinner.setSelection(array_spinner.getPosition("15 días"));
            }
            if (datos.getStatus() == 7) {
                spinner.setSelection(array_spinner.getPosition("7 días"));
            }



        }
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        buttonSAVE.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (cantidadImagenes.getText().toString().matches("") || Integer.valueOf(cantidadImagenes.getText().toString()) <= 2 ) {
                    Toast.makeText(PublicidadActivity.this, "Ingresa una cantidad de imagenes a desplegar", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (tiempoImagenes.getText().toString().matches("") ||  Integer.valueOf(tiempoImagenes.getText().toString()) <= 3000 ) {
                    Toast.makeText(PublicidadActivity.this, "Ingresa un tiempo de imagenes valido", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (TiempoInactividad.getText().toString().matches("") ||  Integer.valueOf(TiempoInactividad.getText().toString()) <= 3000) {
                    Toast.makeText(PublicidadActivity.this, "Ingresa un tiempo de inactividad valido", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (contrasenaAdmin.getText().toString().matches("")) {
                    Toast.makeText(PublicidadActivity.this, "Ingrese una contraseña de acceso al panel de administracion", Toast.LENGTH_SHORT).show();
                    return;
                }

                Configuration datos = configurationData.getConfiguration("RETAIL");
                newConfiguration.setCantImagenes(Integer.valueOf(cantidadImagenes.getText().toString()));
                newConfiguration.setTiempoImagen(Integer.valueOf(tiempoImagenes.getText().toString()));
                newConfiguration.setTiempoInactividad(Integer.valueOf(TiempoInactividad.getText().toString()));
                newConfiguration.setContrasenaAdmin(Integer.valueOf(contrasenaAdmin.getText().toString()));

                newConfiguration.setLogActivo(Integer.valueOf(datos.getCantImagenes().toString()));
                newConfiguration.setLogNroDias(Integer.valueOf(datos.getTiempoImagen().toString()));

                newConfiguration.setRebootHour(10);
                newConfiguration.setRebootMinute(0);

                newConfiguration.setStockGrilla(0);
                if (checkboxGrilla.isChecked()){
                    newConfiguration.setStockGrilla(1);
                }
                newConfiguration.setMuestraPublicidad(0);
                if (checkbox.isChecked()){
                    newConfiguration.setMuestraPublicidad(1);
                }
                newConfiguration.setLogActivo(0);
                if (checkboxLoog.isChecked()){
                    newConfiguration.setLogActivo(1);
                }

                newConfiguration.setVozPrecio(0);
                if (checkboxVoz.isChecked()){
                    newConfiguration.setVozPrecio(1);
                }

                newConfiguration.setLogNroDias(30);
                String texto = spinner.getSelectedItem().toString().toUpperCase();
                if (Objects.equals(texto, new String("15 días"))) {
                    newConfiguration.setLogNroDias(15);
                }
                if (Objects.equals(texto, new String("7 días"))) {
                    newConfiguration.setLogNroDias(7);
                }

                newConfiguration.setEmpId(theId);
                /* DEFAULT SETTINGS */
                newConfiguration.setIPDesc(datos.getIPDesc().toString());
                newConfiguration.setPORTDesc(datos.getPORTDesc().toString());
                newConfiguration.setTipo(datos.getTipo().toString());
                newConfiguration.setLocal(datos.getLocal().toString());
                newConfiguration.setConsultor(Long.valueOf(datos.getConsultor().toString()));
                newConfiguration.setNombreLocal(datos.getNombreLocal().toString());
                newConfiguration.setUrlWs(datos.getUrlWs().toString());
                newConfiguration.setAbrevLocal(datos.getAbrevLocal().toString());
                newConfiguration.setTprecioPantalla(Integer.valueOf(datos.getTprecioPantalla().toString()));
                newConfiguration.setSkin(Integer.valueOf(datos.getSkin().toString()));
                newConfiguration.setStockMinimo(Integer.valueOf(datos.getStockMinimo().toString()));
                newConfiguration.setConsultaStock(Integer.valueOf(datos.getConsultaStock().toString()));
                newConfiguration.setStatus(Integer.valueOf(datos.getStatus().toString()));
                newConfiguration.setTimeSocket(datos.getTimeSocket());
                newConfiguration.setTimeWS(datos.getTimeWS());
                /* FIN DE DEFAULT SETTINGS */

                configurationData.updateConfiguration(newConfiguration);

                Toast t = Toast.makeText(PublicidadActivity.this,"Registro Guardado",Toast.LENGTH_SHORT);
                t.show();
            }
        });

        buttonSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent x = new Intent(PublicidadActivity.this, retailActivity.class);
                startActivity(x);
            }
        });

        buttonHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                List<String> mStringEjecucion = imagenesYvideos(getApplicationContext());
                String[] strings = new String[mStringEjecucion.size()];
                strings = mStringEjecucion.toArray(strings);
                //stringsCompare = new String[mStringEjecucion.size()];
                /*
                Bundle bundle = new Bundle();
                bundle.putStringArrayList("mStringEjecucion",(ArrayList<String>)mStringEjecucion);
                bundle.putStringArray("strings", strings);
                */
                Intent i = new Intent(PublicidadActivity.this, SplashActivity.class);
                //i.putExtras(bundle);
                startActivity(i);
                finish();
                /*
                Intent i = new Intent(PublicidadActivity.this, WelcomeActivity.class);
                startActivity(i);
                */
            }
        });

    }

}
