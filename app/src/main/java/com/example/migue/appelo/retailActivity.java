package com.example.migue.appelo;

import android.content.Intent;
import android.os.Bundle;

import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputFilter;
import android.text.Spanned;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.migue.appelo.DB.ConfigurationOperations;
import com.example.migue.appelo.Model.Configuration;
import com.example.migue.appelo.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static com.example.migue.appelo.Utils.getConfiguration;
import static com.example.migue.appelo.Utils.imagenesYvideos;

public class retailActivity extends AppCompatActivity
 {
     private EditText editTextIP,editTextPORT,editTextPRECIOPANTALLA,editTextCODIGOLOCAL,editTextNOMBRELOCAL,editTextCODIGOCONSULTOR,editTextURLWS,editTextSKINSELECTED,editTextStockMinimo,editTextAbrevLocal;
     private EditText editTextTimeoutWS,editTextTimeoutSocket;
     private Long theId;
     private Button buttonDB,buttonHome,buttonPublicidad;
     private Button buttonSAVE;
     private Configuration newConfiguration;
     private ConfigurationOperations configurationData;
     private String accion = "INSERT";
     private ImageView imageViewParis, imageViewJohnson;
     private Spinner spinner;
     public Configuration datos;
     private ImageButton imageButtonRetail,imageButtonSupermarket;
     private CheckBox checkbox;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_retail);

        editTextIP = (EditText) findViewById(R.id.editTextIP);
        editTextPORT = (EditText) findViewById(R.id.editTextPORT);
        editTextSKINSELECTED = (EditText) findViewById(R.id.editTextSKINSELECTED);
        editTextCODIGOCONSULTOR = (EditText) findViewById(R.id.editTextCODIGOCONSULTOR);
        editTextNOMBRELOCAL = (EditText) findViewById(R.id.editTextNOMBRELOCAL);
        editTextCODIGOLOCAL = (EditText) findViewById(R.id.editTextCODIGOLOCAL);
        editTextPRECIOPANTALLA = (EditText) findViewById(R.id.editTextPRECIOPANTALLA);
        editTextURLWS = (EditText) findViewById(R.id.editTextURLWS);
        editTextStockMinimo = (EditText) findViewById(R.id.editTextStockMinimo);
        editTextAbrevLocal = (EditText) findViewById(R.id.editTextAbrevLocal);
        checkbox = (CheckBox) findViewById(R.id.checkbox);
        imageViewParis = (ImageView) findViewById(R.id.imageViewParis);
        imageViewJohnson = (ImageView) findViewById(R.id.imageViewJohnson);
        spinner = (Spinner) findViewById(R.id.spinner);
        buttonDB = (Button) findViewById(R.id.buttonDB);
        buttonSAVE = (Button) findViewById(R.id.buttonSAVE);
        buttonHome = (Button) findViewById(R.id.buttonHome);
        buttonPublicidad = (Button) findViewById(R.id.buttonPublicidad);
        editTextTimeoutSocket = (EditText) findViewById(R.id.editTextTimeoutSocket);
        editTextTimeoutWS = (EditText) findViewById(R.id.editTextTimeoutWS);


        setFilter();

        buttonDB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent dbmanager = new Intent(retailActivity.this,AndroidDatabaseManager.class);
                startActivity(dbmanager);
            }
        });

        buttonHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                List<String> mStringEjecucion = imagenesYvideos(getApplicationContext());
                String[] strings = new String[mStringEjecucion.size()];
                strings = mStringEjecucion.toArray(strings);
                //stringsCompare = new String[mStringEjecucion.size()];
                /*
                Bundle bundle = new Bundle();
                bundle.putStringArrayList("mStringEjecucion",(ArrayList<String>)mStringEjecucion);
                bundle.putStringArray("strings", strings);
                */
                Intent i = new Intent(retailActivity.this, SplashActivity.class);
                //i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                //i.putExtras(bundle);
                startActivity(i);
                finish();
                /*
                Intent i = new Intent(retailActivity.this, WelcomeActivity.class);
                startActivity(i);
                */
            }
        });

        buttonPublicidad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent x = new Intent(retailActivity.this, PublicidadActivity.class);
                startActivity(x);
            }
        });





        newConfiguration = new Configuration();
        configurationData = new ConfigurationOperations(this);
        configurationData.open();

        Configuration datos = configurationData.getConfiguration("RETAIL");
        Global.datos = datos;

        if (datos != null) {
            theId = Long.valueOf(datos.getEmpId());
            editTextIP.setText(datos.getIPDesc().toString());
            editTextPORT.setText(datos.getPORTDesc().toString());
            editTextCODIGOLOCAL.setText(datos.getLocal().toString());
            editTextPRECIOPANTALLA.setText(datos.getTprecioPantalla().toString());
            editTextURLWS.setText(datos.getUrlWs().toString());
            editTextCODIGOCONSULTOR.setText(datos.getConsultor().toString());
            editTextNOMBRELOCAL.setText(datos.getNombreLocal().toString());
            editTextStockMinimo.setText(datos.getStockMinimo().toString());
            editTextAbrevLocal.setText(datos.getAbrevLocal());
            editTextTimeoutSocket.setText(datos.getTimeSocket().toString());
            editTextTimeoutWS.setText(datos.getTimeWS().toString());
            if (datos.getConsultaStock() == 1){
                checkbox.setChecked(true);
            }
            if (datos.getSkin() == 1) {
                editTextSKINSELECTED.setText("SKIN PARIS");
            }
            if (datos.getSkin() == 2) {
                editTextSKINSELECTED.setText("SKIN JOHNSON");
            }
            ArrayAdapter<String> array_spinner=(ArrayAdapter<String>)spinner.getAdapter();
            if (datos.getStatus() == 1) {
                spinner.setSelection(array_spinner.getPosition("ACTIVO"));
            } else {
                spinner.setSelection(array_spinner.getPosition("INACTIVO"));
            }

            accion = "UPDATE";
        }


        imageViewParis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editTextSKINSELECTED.setText("SKIN PARIS");
            }
        });
        imageViewJohnson.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editTextSKINSELECTED.setText("SKIN JOHNSON");
            }
        });
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN|WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        buttonSAVE.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                if (editTextIP.getText().toString().matches("")) {
                    Toast.makeText(retailActivity.this, "Ingresa una IP valida", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (editTextPORT.getText().toString().matches("")) {
                    Toast.makeText(retailActivity.this, "Ingresa un PORT valido", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (editTextTimeoutSocket.getText().toString().matches("")) {
                    Toast.makeText(retailActivity.this, "Ingresa un time out valido para el socket", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (editTextPRECIOPANTALLA.getText().toString().matches("")) {
                    Toast.makeText(retailActivity.this, "Ingresa el Tiempo de precio en pantalla", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (editTextCODIGOLOCAL.getText().toString().matches("")) {
                    Toast.makeText(retailActivity.this, "Ingrese un codigo de local", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (editTextCODIGOCONSULTOR.getText().toString().matches("")) {
                    Toast.makeText(retailActivity.this, "Ingresa un codigo de consultor", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (editTextNOMBRELOCAL.getText().toString().matches("")) {
                    Toast.makeText(retailActivity.this, "Ingresa un nombre de local", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (editTextURLWS.getText().toString().matches("")) {
                    Toast.makeText(retailActivity.this, "Ingresa una URL para el webservice", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (editTextTimeoutWS.getText().toString().matches("")) {
                    Toast.makeText(retailActivity.this, "Ingresa un time out valido para el WS", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (editTextAbrevLocal.getText().toString().matches("")) {
                    Toast.makeText(retailActivity.this, "Ingresa un abrev de local", Toast.LENGTH_SHORT).show();
                    return;
                }


                newConfiguration.setIPDesc(editTextIP.getText().toString());
                newConfiguration.setPORTDesc(editTextPORT.getText().toString());
                newConfiguration.setTipo("RETAIL");

                newConfiguration.setLocal(editTextCODIGOLOCAL.getText().toString());
                newConfiguration.setConsultor(Long.valueOf(editTextCODIGOCONSULTOR.getText().toString()));
                newConfiguration.setNombreLocal(editTextNOMBRELOCAL.getText().toString());
                newConfiguration.setUrlWs(editTextURLWS.getText().toString());
                newConfiguration.setAbrevLocal(editTextAbrevLocal.getText().toString());
                newConfiguration.setTprecioPantalla(Integer.valueOf(editTextPRECIOPANTALLA.getText().toString()));

                newConfiguration.setTimeSocket(Integer.valueOf(editTextTimeoutSocket.getText().toString()));
                newConfiguration.setTimeWS(Integer.valueOf(editTextTimeoutWS.getText().toString()));

                newConfiguration.setSkin(2);
                if (editTextStockMinimo.getText().toString().matches("")) {
                    newConfiguration.setStockMinimo(0);
                } else {
                    newConfiguration.setStockMinimo(Integer.valueOf(editTextStockMinimo.getText().toString()));
                }
                newConfiguration.setConsultaStock(0);
                if (checkbox.isChecked()){
                    newConfiguration.setConsultaStock(1);
                }

                if (Objects.equals(editTextSKINSELECTED.getText().toString(), "SKIN PARIS")){
                    newConfiguration.setSkin(1);
                }

                newConfiguration.setStatus(0);
                String texto = spinner.getSelectedItem().toString().toUpperCase();
                if (Objects.equals(texto, new String("ACTIVO"))) {
                    newConfiguration.setStatus(1);
                }
                newConfiguration.setCantImagenes(5);
                newConfiguration.setTiempoImagen(6000);
                newConfiguration.setTiempoInactividad(10000);
                newConfiguration.setMuestraPublicidad(0);
                newConfiguration.setStockGrilla(0);

                newConfiguration.setRebootMinute(0);
                newConfiguration.setRebootHour(10);

                newConfiguration.setLogActivo(0);
                newConfiguration.setLogNroDias(30);

                if (accion == "INSERT"){
                    configurationData.addConfiguration(newConfiguration);
                } else {
                    Configuration datos = configurationData.getConfiguration("RETAIL");
                    /* DEFAULT PUBLICIDAD */
                    newConfiguration.setCantImagenes(Integer.valueOf(datos.getCantImagenes().toString()));
                    newConfiguration.setTiempoImagen(Integer.valueOf(datos.getTiempoImagen().toString()));
                    newConfiguration.setTiempoInactividad(Integer.valueOf(datos.getTiempoInactividad().toString()));
                    newConfiguration.setMuestraPublicidad(datos.getMuestraPublicidad());
                    newConfiguration.setStockGrilla(datos.getStockGrilla());
                    newConfiguration.setVozPrecio(datos.getVozPrecio());
                    newConfiguration.setLogActivo(Integer.valueOf(datos.getLogActivo().toString()));
                    newConfiguration.setLogNroDias(Integer.valueOf(datos.getLogNroDias().toString()));

                    newConfiguration.setContrasenaAdmin(Integer.valueOf(datos.getContrasenaAdmin().toString()));

                    newConfiguration.setRebootMinute(datos.getRebootMinute());
                    newConfiguration.setRebootHour(datos.getRebootHour());


                    newConfiguration.setEmpId(theId);
                    configurationData.updateConfiguration(newConfiguration);
                }
                //configurationData.addEmployee(newConfiguration);
                Toast t = Toast.makeText(retailActivity.this,"Registro Guardado",Toast.LENGTH_SHORT);
                t.show();

            }
        });

    }

     public void setFilter(){
         InputFilter[] filters = new InputFilter[1];
         filters[0] = new InputFilter() {
             @Override
             public CharSequence filter(CharSequence source, int start,
                                        int end, Spanned dest, int dstart, int dend) {
                 if (end > start) {
                     String destTxt = dest.toString();
                     String resultingTxt = destTxt.substring(0, dstart) +
                             source.subSequence(start, end) +
                             destTxt.substring(dend);
                     if (!resultingTxt.matches ("^\\d{1,3}(\\." +
                             "(\\d{1,3}(\\.(\\d{1,3}(\\.(\\d{1,3})?)?)?)?)?)?")) {
                         return "";
                     } else {
                         String[] splits = resultingTxt.split("\\.");
                         for (int i=0; i<splits.length; i++) {
                             if (Integer.valueOf(splits[i]) > 255) {
                                 return "";
                             }
                         }
                     }
                 }
                 return null;
             }
         };
         editTextIP.setFilters(filters);
     }







}
