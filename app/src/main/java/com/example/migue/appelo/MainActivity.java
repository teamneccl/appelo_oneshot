package com.example.migue.appelo;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.media.AudioManager;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.StrictMode;
import android.speech.tts.TextToSpeech;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.Html;
//import android.util.Log;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.VideoView;

import com.example.migue.appelo.DB.ConfigurationOperations;
import com.example.migue.appelo.Model.Configuration;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.Socket;
import java.net.URL;
import java.net.UnknownHostException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.SortedSet;
import java.util.Timer;
import java.util.TimerTask;
import java.util.TreeMap;
import java.util.TreeSet;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import static com.example.migue.appelo.Utils.LogRegister;
import static com.example.migue.appelo.Utils.getConfiguration;
import static com.example.migue.appelo.Utils.getXMLString;
import static com.example.migue.appelo.Utils.getLocalIpAddress;
import static com.example.migue.appelo.Utils.imagenesYvideos;
import static com.example.migue.appelo.Utils.xmlEnvelop;


public class MainActivity extends AppCompatActivity {
    private ConfigurationOperations configurationData;
    public TextView TextViewRCV,textViewTA,textViewRA,textViewDesc,textViewTB,textViewRB,textViewTG,textViewEND,textViewMSGStock,textViewVersion,textViewSKU,textViewENDPre,textViewBandera,textViewBanderaGreen;
    public Integer status = 0;
    public EditText editTextMSG,editTextMSGV;
    public Socket client;
    public ScrollView scrollvertical;
    private VideoView videoView;
    private Button buttonConn;
    public String codeMSG,urlWs,url,testSOAP,inputString,valorHablado,looper,color,codigo,sku,colore,xml,picture,globalCadena,stock = "";
    public String[] skus, colors;
    public LinearLayout ly,ly2,linearLayoutTabla, layoutTabla;
    public ConstraintLayout constraintLayout;
    private CoordinatorLayout mainLayout;
    public Dialog publicityDialog;
    private ImageView imageView,imageFlechaAbajo;
    private String miIp, mensajeComando,cEmpresa,cLocal,cFecha,cHora;
    public TextToSpeech tts;
    public Configuration datos;
    public Bundle bundle;
    public Date cDate;
    private Integer cont,i = 0;
    public Locale spanish = new Locale("es", "CL");
    public DecimalFormatSymbols simbolos = DecimalFormatSymbols.getInstance(spanish);
    public NumberFormat nf = NumberFormat.getNumberInstance(spanish);
    public DecimalFormat formatea = (DecimalFormat)nf;
    public Thread t,tr;
    public InetSocketAddress inetAddress;
    public PrintWriter printWriter;
    public Timer timer2, timer3,timer0;
    public Runnable thRead,r,ra;
    public Bitmap myBitmap;
    private List<String> mStringEjecucion = new ArrayList<String>();
    private String[] strings;
    public List<String> mStringEjecucionCompare = new ArrayList<String>();
    public long tiempoPublicidad,bandera;
    public ConnectivityManager connectivityManager;
    public NetworkInfo activeNetworkInfo;



    Handler handler=new Handler(){
        @Override
        public void handleMessage(Message msg) {
            if (msg.what==100){
                i=0;
            }
        }
    };

    private void setText(final TextView text,final String value){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                text.setText(value);
            }
        });
    }

    public boolean isNetworkAvailable() {
        connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //editTextMSGV = findViewById(R.id.editTextMSGV);
        mainLayout =  findViewById(R.id.mainLayout);
        textViewSKU = findViewById(R.id.textViewSKU);
        buttonConn = findViewById(R.id.buttonConn);
        textViewBanderaGreen = findViewById(R.id.textViewBanderaGreen);
        TextViewRCV = findViewById(R.id.TextViewRCV);
        textViewBandera = findViewById(R.id.textViewBandera);
        imageView = findViewById(R.id.imageView);
        textViewTA = findViewById(R.id.textViewTA);
        textViewRA = findViewById(R.id.textViewRA);
        textViewDesc = findViewById(R.id.textViewDesc);
        textViewTB = findViewById(R.id.textViewTB);
        textViewRB = findViewById(R.id.textViewRB);
        scrollvertical = findViewById(R.id.scrollvertical);
        textViewTG = findViewById(R.id.textViewTG);
        textViewENDPre = findViewById(R.id.textViewENDPre);
        imageFlechaAbajo = findViewById(R.id.imageFlechaAbajo);
        textViewEND = findViewById(R.id.textViewEND);
        textViewMSGStock = findViewById(R.id.textViewMSGStock);
        textViewVersion = findViewById(R.id.textViewVersion);
        editTextMSG = findViewById(R.id.editTextMSG);
        constraintLayout = findViewById(R.id.constraintLayout);
        ly = findViewById(R.id.layout1y);
        linearLayoutTabla = findViewById(R.id.linearLayoutTabla);
        layoutTabla = findViewById(R.id.layoutTabla);
        ly2 = findViewById(R.id.layout1y2);
        formatea.applyPattern("###,###");
        cont = 0;

        Thread.setDefaultUncaughtExceptionHandler(new MyExceptionHandler(this));


        clear(1);
        try {
            miIp = getLocalIpAddress(1).toString();
            //miIp = "00.00.00.00";
        } catch (Exception e) {
            miIp = "00.00.00.00";
        }

        //

        //configurationData = getConfiguration(this);
        //datos = configurationData.getConfigurationByStatus(1);
        datos = Global.datos;
        try {
            mStringEjecucion = Global.mStringEjecucion;
            strings = Global.strings;
            globalCadena = "init";
        } catch (Exception e) {

        }


       //show("1234567890123");



        if (Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        if (datos != null) {
            if (String.valueOf(Build.SERIAL).equals(Global.serialDevice)) {
                if (mStringEjecucion.size() > 0 && datos.getMuestraPublicidad().intValue() == 1) {
                    //Log.i("publicidad",String.valueOf(mStringEjecucion));
                    startPublicity();
                }
            }
            //Log.i("datos","si");
            try {
                textViewVersion.setText(Html.fromHtml(datos.getNombreLocal() +"<br>Version 1.7 - " + datos.getAbrevLocal() + miIp.split("[.]")[3].toString()));
            } catch (Exception e) {
                textViewVersion.setText(Html.fromHtml(datos.getNombreLocal() +"<br>Version 1.7 - " + datos.getAbrevLocal() + "00"));
            }

            urlWs =  datos.getUrlWs().toString();
            //checkStock = Integer.valueOf(datos.getConsultaStock());

            if (datos.getSkin().intValue() == 1) {
                mainLayout.setBackgroundColor(Color.parseColor("#00AAEF"));
                imageView.setImageResource(R.drawable.alogo1);
                cEmpresa = "01";
            } else {
                cEmpresa = "02";
                mainLayout.setBackgroundColor(Color.parseColor("#FF6600"));
                imageView.setImageResource(R.drawable.blogo1);
            }
        }

        editTextMSG.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (String.valueOf(Build.SERIAL).equals(Global.serialDevice)) {
                    if (editable.length() > 12) {
                        //Log.i("entro","entro a leer");
                        editTextMSG.setEnabled(false);
                        if (editable.equals("0001110009997")) {
                            stopHandler();
                            Intent i = new Intent(MainActivity.this, retailActivity.class);
                            startActivity(i);
                        } else {
                            if (datos != null) {
                                //Log.i("entro","penso");

                                if (isNetworkAvailable() == true) {
                                    //TextViewRCV.setText("Consultando el precio ...");
                                    //if (TextViewRCV.getVisibility() == View.VISIBLE) {
                                    handler.removeCallbacks(ra);
                                    clear(0);
                                    //}
                                    show(editTextMSG.getText().toString());
                                } else {
                                    TextViewRCV.setText("Sin conexion de red");
                                    timer3 = new Timer();
                                    editTextMSG.setText("");
                                    editTextMSG.setEnabled(true);
                                    timer3.schedule(new TimerTask() {
                                        public void run() {
                                            setText(TextViewRCV, "Acerque producto para consultar");
                                            timer3.cancel();
                                        }
                                    }, 2000);
                                }


                            }
                        }

                    }
                }
            }
        });

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (String.valueOf(Build.SERIAL).equals(Global.serialDevice)) {
                    if (i == 0) {
                        ++i;
                        handler.sendEmptyMessageDelayed(100, 2000); // 3000 equal 3sec , you can set your own limit of secs
                    } else if (i == 2) {
                        //Toast.makeText(WelcomeActivity.this, "Three Touch Clicked" , Toast.LENGTH_SHORT).show();
                        i = 0;
                        handler.removeMessages(100);

                        final AlertDialog.Builder alert = new AlertDialog.Builder(MainActivity.this);
                        final EditText edittext = new EditText(MainActivity.this);
                        edittext.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_VARIATION_PASSWORD);
                        edittext.setFilters(new InputFilter[]{new InputFilter.LengthFilter(10)});
                        alert.setMessage("");
                        alert.setTitle("Cencosud admin panel");


                        LinearLayout layout = new LinearLayout(MainActivity.this);
                        layout.setOrientation(LinearLayout.VERTICAL);
                        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                        params.setMargins(150, 0, 150, 0);
                        layout.addView(edittext, params);
                        alert.setView(layout);

                        alert.setPositiveButton("Enviar", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                String YouEditTextValue = edittext.getText().toString();
                                //Intent i = new Intent(MainActivity.this, retailActivity.class);
                                //startActivity(i);
                                if (YouEditTextValue.equals(Global.datos.getContrasenaAdmin().toString())) {
                                    //stopHandler();
                                    Intent i = new Intent(MainActivity.this, retailActivity.class);
                                    startActivity(i);
                                } else {
                                    getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                                    dialog.dismiss();
                                }
                            }
                        });

                        alert.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                            }
                        });

                        alert.setOnCancelListener(new DialogInterface.OnCancelListener() {
                            @Override
                            public void onCancel(DialogInterface dialog) {
                                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                            }
                        });

                        alert.show();

                    } else
                        ++i;
                }
            }
        });

    }

    public void startHandler() {

        if (datos != null) {
            handler.postDelayed(r, datos.getTiempoInactividad()); //for 5 minutes
        }

    }

    public void stopHandler() {
        handler.removeCallbacks(r);
        //handler.removeCallbacks(ro);
        handler.removeCallbacksAndMessages(null);
        //Log.i("si","detuvo");

    }

    private void startPublicity() {
        r = new Runnable() {
            @Override
            public void run() {
                picture = "";
                publicityDialog = new Dialog(MainActivity.this);
                publicityDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                publicityDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                // TEST

                // FIN DE TEST
                publicityDialog.setOnCancelListener(
                        new DialogInterface.OnCancelListener() {
                            @Override
                            public void onCancel(DialogInterface dialog) {
                                TextViewRCV.setVisibility(View.VISIBLE);
                                if (timer2!= null) {
                                    timer2.cancel();
                                    //timer2=null;
                                }
                                try {

                                    videoView.stopPlayback();
                                } catch (Exception e) {

                                }
                                try {
                                    myBitmap.recycle();
                                    myBitmap = null;
                                } catch (Exception e) {

                                }
                                try {

                                    stopHandler();
                                    editTextMSG.setEnabled(true);
                                    globalCadena = "init";
                                    mStringEjecucionCompare.clear();
                                    startHandler();
                                } catch (Exception e) {

                                }
                            }
                        }
                );
                if (datos.getSkin().intValue() == 2){
                    publicityDialog.getWindow().setBackgroundDrawableResource(R.color.colorOrangeCencosud);
                } else {
                    publicityDialog.getWindow().setBackgroundDrawableResource(R.color.colorBlueCensosud);
                }
                if (globalCadena.equals("init")) {
                    mStringEjecucionCompare.clear();
                    mStringEjecucionCompare.add(strings[0]);
                    globalCadena = "init0";
                    picture = strings[0];
                } else {
                    for (int i = 0; i < strings.length; i++) {
                        Boolean bool = mStringEjecucionCompare.contains(strings[i]) ? true : false;
                        if (mStringEjecucionCompare.size() > datos.getCantImagenes()) {
                            globalCadena = "init";
                            break;
                        }
                        if (mStringEjecucion.size() == mStringEjecucionCompare.size()) {
                            globalCadena = "init";
                            break;
                        }
                        if (bool == false) {
                            mStringEjecucionCompare.add(strings[i]);
                            picture = strings[i];
                            break;
                        }
                    }
                }
                File miFile = new File(picture);
                if (miFile.exists()) {

                    if (picture.contains(".mp4")) {
                        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
                        retriever.setDataSource(getApplicationContext(), Uri.fromFile(miFile));
                        String time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
                        long timeInMillisec = Long.parseLong(time);
                        tiempoPublicidad = timeInMillisec;
                        //Log.i("Tiempo publicidad",String.valueOf(tiempoPublicidad));

                        retriever.release();

                        publicityDialog.setContentView(getLayoutInflater().inflate(R.layout.video_layout
                                , null));
                        videoView = publicityDialog.findViewById(R.id.videoView);

                        Uri uri = Uri.parse(picture);
                        videoView.setVideoURI(uri);
                        videoView.setZOrderOnTop(true);

                        MediaController mc = new MediaController(MainActivity.this);
                        videoView.setMediaController(mc);
                        videoView.start();
                    } else {
                        tiempoPublicidad = datos.getTiempoImagen();
                        publicityDialog.setContentView(getLayoutInflater().inflate(R.layout.image_layout
                                , null));
                        ImageView image = publicityDialog.findViewById(R.id.image_dialog);

                        BitmapFactory.Options options = new BitmapFactory.Options();
                        options.inSampleSize = 1;

                        myBitmap = BitmapFactory.decodeFile(miFile.getAbsolutePath(),options);
                        image.setImageBitmap(myBitmap);
                    }
                } else {
                    publicityDialog.dismiss();
                    handler.postDelayed(r, 100);
                }


                if (picture.contains(".mp4") || picture.contains(".jpg") || picture.contains(".png") || picture.contains(".bmp")) {
                    editTextMSGV = publicityDialog.findViewById(R.id.editTextMSGV);

                    TextView textBotton = publicityDialog.findViewById(R.id.textBotton);
                    textBotton.setText("Acerque producto para consultar");

                    if (picture.contains(".mp4")) {
                        videoView.setOnTouchListener(new View.OnTouchListener() {
                            @Override
                            public boolean onTouch(View view, MotionEvent motionEvent) {
                                return false;
                            }
                        });
                    }


                    editTextMSGV.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                        }

                        @Override
                        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                        }

                        @Override
                        public void afterTextChanged(Editable editable) {
                            if (String.valueOf(Build.SERIAL).equals(Global.serialDevice)) {
                                if (editable.length() > 12) {
                                    //imageFlechaAbajo.setVisibility(View.INVISIBLE);
                                    editTextMSGV.setEnabled(false);
                                    if (editable.equals("0001110009997")) {
                                        //stopHandler();
                                        Intent i = new Intent(MainActivity.this, retailActivity.class);
                                        startActivity(i);
                                    } else {

                                        if (datos != null) {
                                            //Log.i("entro","penso");
                                            try {
                                                timer2.cancel();
                                            } catch (Exception e) {

                                            }
                                            try {
                                                videoView.stopPlayback();
                                            } catch (Exception e) {

                                            }
                                            try {
                                                myBitmap.recycle();
                                                myBitmap = null;
                                            } catch (Exception e) {

                                            }
                                            try {
                                                //Log.i("nose","paso dismiss");
                                                editTextMSG.setEnabled(false);
                                                publicityDialog.dismiss();
                                                globalCadena = "init";
                                                mStringEjecucionCompare.clear();
                                                if (isNetworkAvailable() == true) {
                                                    //TextViewRCV.setText("Consultando el precio ...");
                                                    handler.removeCallbacks(ra);
                                                    clear(0);
                                                    show(editTextMSGV.getText().toString());
                                                } else {
                                                    TextViewRCV.setText("Sin conexion de red");
                                                    timer3 = new Timer();
                                                    editTextMSG.setEnabled(true);
                                                    timer3.schedule(new TimerTask() {
                                                        public void run() {
                                                            setText(TextViewRCV, "Acerque producto para consultar");
                                                            //TextViewRCV.setText("Acerca tu producto para conocer su precio");
                                                            //textViewWelcome.setText("Acerca tu producto para conocer su precio");
                                                            timer3.cancel();
                                                        }
                                                    }, 2000);
                                                }
                                            } catch (Exception e) {

                                            }


                                            //showPrice();
                                            //returnWelcome();

                                            //conexionSocket(editTextMSG.getText().toString().substring(0,13));
                                        }
                                    }
                                    //editTextMSG.setText("");
                                }
                            }
                        }
                    });
                    //editTextMSGV.requestFocus();



                    TextViewRCV.setVisibility(View.INVISIBLE);
                    publicityDialog.show();



                    editTextMSGV.requestFocus();

                    timer2= new Timer();
                    timer2.schedule(new TimerTask() {
                        public void run() {
                            publicityDialog.dismiss();
                            try {
                                if (myBitmap != null) {
                                    myBitmap.recycle();
                                    myBitmap = null;
                                }
                            } catch (Exception e) {

                            }


                            timer2.cancel();
                        }
                    }, tiempoPublicidad - 100);
                    handler.postDelayed(r, tiempoPublicidad);
                    //editTextMSGV.requestFocus();
                }
                //publicityDialog.show();
            }
        };
        startHandler();
    }

    private void clear(Integer entero){

        /*
        final Handler handlerAct = new Handler();
        handlerAct.postDelayed(new Runnable() {
                                   @Override
                                   public void run() {
                                       //editTextMSG.setEnabled(true);
                                       editTextMSG.setText("");
                                       editTextMSG.setText("2019664200983");

                                   }
                               },4000);
        */

        textViewMSGStock.setText("");
        textViewMSGStock.setVisibility(View.INVISIBLE);
        linearLayoutTabla.setVisibility(View.INVISIBLE);
        layoutTabla.setVisibility(View.INVISIBLE);
        //constraintLayout.setVisibility(View.INVISIBLE);
        if(( ly).getChildCount() > 0)
            ( ly).removeAllViews();
        if(( ly2).getChildCount() > 0)
            ( ly2).removeAllViews();

        textViewDesc.setText("");
        textViewDesc.setVisibility(View.INVISIBLE);
        textViewSKU.setText("");
        textViewSKU.setVisibility(View.INVISIBLE);
        textViewTG.setText("");
        textViewTG.setVisibility(View.INVISIBLE);
        textViewEND.setVisibility(View.INVISIBLE);
        textViewEND.setText("");
        textViewENDPre.setVisibility(View.INVISIBLE);
        textViewENDPre.setText("");
        textViewTA.setVisibility(View.INVISIBLE);
        textViewTA.setText("");
        textViewRA.setVisibility(View.INVISIBLE);
        textViewRA.setText("");
        textViewTB.setVisibility(View.INVISIBLE);
        textViewTB.setText("");
        textViewRB.setVisibility(View.INVISIBLE);
        textViewRB.setText("");
        textViewBandera.setVisibility(View.INVISIBLE);
        textViewBanderaGreen.setVisibility(View.INVISIBLE);
        //TextViewRCV.setVisibility(View.INVISIBLE);
        TextViewRCV.setTextSize(TypedValue.COMPLEX_UNIT_SP,55);
        TextViewRCV.setVisibility(View.VISIBLE);
        imageFlechaAbajo.setVisibility(View.VISIBLE);
        if (entero == 1) {
            if (!String.valueOf(Build.SERIAL).equals(Global.serialDevice)) {
                TextViewRCV.setText("Aplicación no autorizada");
                //TextViewRCV.setText(String.valueOf(Build.SERIAL));

            } else {
                TextViewRCV.setText("Acerque producto para consultar");
            }
            imageFlechaAbajo.setVisibility(View.VISIBLE);
            //TextViewRCV.setText(String.valueOf(Build.SERIAL));
        } else {
            TextViewRCV.setText("");
            imageFlechaAbajo.setVisibility(View.INVISIBLE);
        }

        if (Integer.valueOf(MainActivity.this.getResources().getConfiguration().orientation) == 2) {
            ConstraintSet set = new ConstraintSet();
            // reagrupados la vista ya que no tiene stock
            set.clone(constraintLayout);
            set.clear(TextViewRCV.getId(), ConstraintSet.TOP);
            set.applyTo(constraintLayout);
            set.clone(constraintLayout);
            set.connect(TextViewRCV.getId(), ConstraintSet.TOP, ConstraintSet.PARENT_ID, ConstraintSet.TOP);
            set.connect(TextViewRCV.getId(), ConstraintSet.BOTTOM, ConstraintSet.PARENT_ID, ConstraintSet.BOTTOM);
            set.applyTo(constraintLayout);
        }

    }

    private void showText(){
        //Log.i("show","text");
        //layoutTabla.setVisibility(View.VISIBLE);
        imageFlechaAbajo.setVisibility(View.INVISIBLE);
        textViewDesc.setVisibility(View.VISIBLE);
        textViewSKU.setVisibility(View.VISIBLE);
        textViewTG.setVisibility(View.VISIBLE);
        textViewEND.setVisibility(View.VISIBLE);
        textViewTA.setVisibility(View.VISIBLE);
        textViewRA.setVisibility(View.VISIBLE);
        textViewTB.setVisibility(View.VISIBLE);
        textViewRB.setVisibility(View.VISIBLE);
        textViewMSGStock.setVisibility(View.VISIBLE);
    }

    private void returnWelcome() {
        /*
        final Handler handlerAct = new Handler();
        handlerAct.postDelayed(new Runnable() {
            @Override
            public void run() {
                //editTextMSG.setEnabled(true);
                editTextMSG.setText("");
                editTextMSG.setText("2019664200983");

            }
        },3000);
        */
        ra = new Runnable() {
            @Override
            public void run() {
                clear(1);
                if (mStringEjecucion.size() > 0 && datos != null) {
                    if (datos.getMuestraPublicidad().intValue() == 1) {
                        startHandler();
                    }


                }
                if (datos.getConsultaStock().intValue() == 1) {
                    if (Integer.valueOf(MainActivity.this.getResources().getConfiguration().orientation) == 2) {
                        ConstraintSet set = new ConstraintSet();
                        // reagrupados la vista ya que no tiene stock
                        set.clone(constraintLayout);
                        set.clear(TextViewRCV.getId(), ConstraintSet.TOP);
                        set.applyTo(constraintLayout);
                        set.clone(constraintLayout);
                        set.connect(TextViewRCV.getId(), ConstraintSet.TOP, ConstraintSet.PARENT_ID, ConstraintSet.TOP);
                        set.connect(TextViewRCV.getId(), ConstraintSet.BOTTOM, ConstraintSet.PARENT_ID, ConstraintSet.BOTTOM);
                        set.applyTo(constraintLayout);
                    }
                }
            }
        };
        handler.postDelayed(ra, datos.getTprecioPantalla());
        /*
        final Handler handlerAct = new Handler();
        handlerAct.postDelayed(new Runnable() {
            @Override
            public void run() {
                //editTextMSG.setEnabled(true);
                handler.removeCallbacks(ra);
                editTextMSG.setText("");
                editTextMSG.setText("2019664200983");

            }
        },6000);
    */
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Runtime.getRuntime().gc();
        //System.exit(0);
    }

    private void show(String codemsg) {
        stopHandler();
        //textViewSKU.setText(Html.fromHtml("SKU" + "<br>" + codemsg));
        codeMSG = codemsg;
        cDate = new Date();
        cLocal = "00000".substring(datos.getLocal().toString().length()) + datos.getLocal().toString();
        cFecha = new SimpleDateFormat("yyyyMMdd").format(cDate);
        cHora = new SimpleDateFormat("hhmmss").format(cDate);
        miIp = miIp + "                    ".substring(miIp.length());
        mensajeComando = cEmpresa + cLocal + cFecha + cHora + miIp + "00" +  codeMSG;
        TextViewRCV.setVisibility(View.VISIBLE);
        TextViewRCV.setText("CONSULTANDO PRODUCTO ...");
        if (datos != null) {
            MiTareaAsincronaDialog tarea2 = new MiTareaAsincronaDialog();
            tarea2.execute(mensajeComando);
        }
    }

    private class MiTareaAsincronaDialog extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            client = new Socket();
            int Timeout = datos.getTimeSocket();
            inetAddress = new InetSocketAddress(datos.getIPDesc().toString(),Integer.valueOf(datos.getPORTDesc().toString()));
            //inetAddress = new InetSocketAddress("192.168.43.239",15002);
            try {
                //Log.i("tag","entrando;");
                client.connect(inetAddress,Timeout);
                //Log.i("tag","despues de entrando;");

                printWriter = new PrintWriter(client.getOutputStream());
                printWriter.write(params[0]);
                printWriter.flush();
                //Log.i("tag","conecto1;");

                byte[] buffer = new byte[4096];
                InputStream dis = new DataInputStream(new BufferedInputStream(client.getInputStream()));
                final byte[] inputData = new byte[4096];
                dis.read(inputData);
                t = new Thread(new Runnable() {
                    //thRead = new Runnable() {
                    @Override
                    public void run() {
                        inputString = new String(inputData).trim();
                        LogRegister(getApplicationContext(), "Socket", mensajeComando, inputString);

                    }
                });

                t.start();
                t.join();
                //Log.e("socket","entro a socket");
            } catch (IOException ex) {
                //Log.i("excepcion",toString(ex));
                ex.printStackTrace();
                return "errorConexion";
                //inputString = "00APROBADO                      PANTALON            000000328500000022850000001285S20171218054021";

            } catch (InterruptedException e) {

                e.printStackTrace();
                return "errorTry";
                //inputString = "00APROBADO                      PANTALON            000000328500000022850000001285S20171218054021";
            }
            if (datos != null) {

                //Log.e("error","paso1");
                if (datos.getConsultaStock().intValue() == 1) {
                    //Log.e("error","pas2");

                    URL url = null;
                    testSOAP="error";
                    try {
                        url = new URL(datos.getUrlWs());
                        HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                        httpURLConnection.setRequestMethod("POST");
                        httpURLConnection.setRequestProperty("Content-Type", "text/xml; charset=UTF-8");
                        httpURLConnection.setRequestProperty("http://www.cencosud.corp/DSCL_OmniChannel_INT2336/OnlineStock", "OnlineStock");
                        xml = xmlEnvelop(codeMSG, datos.getLocal());

                        httpURLConnection.setDoInput(true);
                        httpURLConnection.setDoOutput(true);
                        httpURLConnection.setConnectTimeout(datos.getTimeWS());
                        httpURLConnection.setReadTimeout(datos.getTimeWS());
                        OutputStream outputStream = httpURLConnection.getOutputStream();

                        BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
                        bufferedWriter.write(xml);
                        bufferedWriter.flush();
                        bufferedWriter.close();
                        outputStream.close();
                        status = httpURLConnection.getResponseCode();
                        //Log.e("stock","entro a stock");
                        //Log.e("stock9",testSOAP.toString());
                        if (status == 200) {
                            //Log.e("stock1","entro a stock");
                            InputStream inputStream = httpURLConnection.getInputStream();
                            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
                            String result = "";
                            String line;
                            while ((line = bufferedReader.readLine()) != null) {
                                result += line;
                            }
                            bufferedReader.close();
                            inputStream.close();
                            testSOAP = result.toString();
                            //testSOAP = Utils.getXMLStringONE();
                            //testSOAP = Utils.getXMLStringTest();
                            if (datos.getLogActivo().intValue() == 1) {
                                LogRegister(getApplicationContext(), "Web Stock", xml, testSOAP);
                            }
                        }

                    } catch (MalformedURLException e) {
                        //Log.e("stock2","entro a stock");
                        //e.printStackTrace();
                        testSOAP = "error";
                        //testSOAP = Utils.getXMLString();
                        //Log.e("error","entro2");
                        if (datos.getLogActivo().intValue() == 1) {
                            LogRegister(getApplicationContext(), "Web Stock", xml, String.valueOf(e));
                        }
                    } catch (IOException e) {
                        //Log.e("stock3","entro a stock");
                        //status = 200;
                        // Log.e("error","entro");
                        //testSOAP = Utils.getXMLStringTest();
                        testSOAP = "error";
                        //testSOAP = Utils.getXMLString();
                        //testSOAP = Utils.getXMLStringONE();
                        //e.printStackTrace();

                        if (datos.getLogActivo().intValue() == 1) {
                            LogRegister(getApplicationContext(), "Web Stock", xml, String.valueOf(e));
                        }
                    } catch (Exception e) {
                        testSOAP = "error";
                        //Log.e("stock4","entro a stock");
                        //testSOAP = Utils.getXMLString();
                        //Log.e("error","entro3");
                        if (datos.getLogActivo().intValue() == 1) {
                            LogRegister(getApplicationContext(), "Web Stock", xml, String.valueOf(e));
                        }

                    }
                }
                //Log.e("stock8",testSOAP.toString());
            }
            return inputString;
        }

        @Override
        protected void onPostExecute(String inputString) {
            int Erropass = 0;
            if (inputString.equals("errorTry")) {
                final Handler handlerAct = new Handler();
                handlerAct.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        editTextMSG.setEnabled(true);
                        editTextMSG.setText("");
                    }
                }, 2000);
                Erropass = 1;
            }
            if (inputString.equals("errorConexion")) {
                final Handler handlerAct = new Handler();
                handlerAct.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        editTextMSG.setEnabled(true);
                        editTextMSG.setText("");
                    }
                }, 2000);
                setText(TextViewRCV, "ERROR DE CONEXION O IP");
                setText(textViewEND, "");
                Erropass = 1;
            }
            if (Erropass == 0) {
                final Handler handlerAct = new Handler();
                handlerAct.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        editTextMSG.setEnabled(true);
                        editTextMSG.setText("");
                    }
                }, 2000);
            }
            if (Erropass == 0) {
                showText();
                if (Objects.equals(inputString.substring(0, 2), "00")) {
                    textViewSKU.setText(Html.fromHtml("SKU" + "<br>" + codeMSG));
                    TextViewRCV.setTextSize(TypedValue.COMPLEX_UNIT_SP, 200);
                    //00APROBADO                      624ALAZV19-257 AUTOL000000599000000059900000003990N20180719122746
                    String normal = simbolos.getCurrencySymbol().toString() + " " + formatea.format(Integer.valueOf(inputString.substring(52, 62))).toString();
                    String vigente = simbolos.getCurrencySymbol().toString() + " " + formatea.format(Integer.valueOf(inputString.substring(62, 72))).toString();
                    String cencosud = simbolos.getCurrencySymbol().toString() + " " + formatea.format(Integer.valueOf(inputString.substring(72, 82))).toString();
                    Integer normalV = Integer.valueOf(inputString.substring(52, 62));
                    Integer vigenteV = Integer.valueOf(inputString.substring(62, 72));
                    Integer cencosudV = Integer.valueOf(inputString.substring(72, 82));
                    if (datos.getConsultaStock().intValue() == 0) {
                        if (Objects.equals(inputString.substring(82, 83), "S")) {
                            textViewEND.setText("Existen ofertas por cantidad. Consulte en caja o dpto. origen.");
                        }
                    } else {
                        if (Objects.equals(inputString.substring(82, 83), "S")) {
                            textViewENDPre.setVisibility(View.VISIBLE);
                            textViewENDPre.setText("Existen ofertas por cantidad. Consulte en caja o dpto. origen.");
                        }
                    }

                    if (((LinearLayout) ly).getChildCount() > 0) {
                        textViewEND.setText("Presiona tu número");
                    }
                    /*
                    if (datos.getConsultaStock().intValue() == 1) {
                        if (datos.getStockGrilla().intValue() == 0) {
                            if (((LinearLayout) ly).getChildCount() == 0) {
                                //textViewBandera.setVisibility(View.VISIBLE);
                                //textViewBandera.setBackgroundColor(Color.parseColor("#FF0000"));
                                textViewEND.setText("Sin información de Stock");
                            }
                        } else {
                            if (((LinearLayout) linearLayoutTabla).getChildCount() == 0) {
                                //textViewBandera.setVisibility(View.VISIBLE);
                                //textViewBandera.setBackgroundColor(Color.parseColor("#FF0000"));
                                textViewEND.setText("Sin información de Stock");
                            }
                        }
                    }
                    */
                    if (cencosudV > 0 && cencosudV < vigenteV && cencosudV < normalV) {
                        TextViewRCV.setText(cencosud);
                        valorHablado = formatea.format(Integer.valueOf(inputString.substring(72, 82))).toString();

                        //textViewTG.setText("Todo medio de pago");
                        if (normalV > vigenteV) {
                            // test de reasignacion - en test
                            textViewTG.setText("Con Tarjeta Cencosud");
                            //textViewTA.setText("Con Tarjeta Cencosud");
                            textViewTA.setText("Todo medio de pago");
                            textViewRA.setText(vigente);

                            textViewTB.setText("Precio Normal");
                            textViewRB.setText(normal);
                        } else {
                            textViewTG.setText("Todo medio de pago");
                            textViewTA.setText("Con Tarjeta Cencosud");
                            textViewRA.setText(vigente);
                        }
                        if (normalV.intValue() == vigenteV.intValue()) {
                            //textViewTG.setText("Todo medio de pago");
                            //textViewTA.setText("Precio Normal");
                            textViewTG.setText("Con Tarjeta Cencosud");
                            textViewTA.setText("Todo medio de pago");
                            textViewRA.setText(vigente);
                        }
                    }
                    if (cencosudV.intValue() == 0) {
                        if (normalV > vigenteV) {
                            //valorHablado = formatea.format(vigenteV).toString();
                            valorHablado = formatea.format(Integer.valueOf(inputString.substring(62, 72))).toString();
                            TextViewRCV.setText(vigente);
                            textViewTG.setText("Todo medio de pago");
                            textViewTA.setText("Precio Normal");
                            textViewRA.setText(normal);
                        } else {
                            valorHablado = formatea.format(Integer.valueOf(inputString.substring(62, 72))).toString();
                            textViewTG.setText("Todo medio de pago");
                            TextViewRCV.setText(vigente);
                        }
                        if (normalV.intValue() == vigenteV.intValue()) {
                            valorHablado = formatea.format(Integer.valueOf(inputString.substring(62, 72))).toString();
                            textViewTG.setText("Todo medio de pago");
                            TextViewRCV.setText(vigente);
                        }
                    }
                    if (cencosudV.intValue() == vigenteV.intValue()) {
                        TextViewRCV.setText(vigente);
                        //valorHablado = formatea.format(vigenteV).toString();
                        valorHablado = formatea.format(Integer.valueOf(inputString.substring(62, 72))).toString();
                        textViewTG.setText("Todo medio de pago");
                    }
                    if (inputString.substring(31, 51).trim().length() == 0) {
                        textViewDesc.setText(Html.fromHtml("Descripción &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + "<br>" + "S/D     "));
                    } else {
                        textViewDesc.setText(Html.fromHtml("Descripción" + "<br>" + inputString.substring(31, 52).trim()) + "     ");
                    }

                } else {
                    if (datos!= null) {
                        if (datos.getConsultaStock().intValue() == 1) {
                            if (Integer.valueOf(MainActivity.this.getResources().getConfiguration().orientation) == 2) {
                                ConstraintSet set = new ConstraintSet();
                                // reagrupados la vista ya que no tiene stock
                                set.clone(constraintLayout);
                                set.clear(TextViewRCV.getId(), ConstraintSet.TOP);
                                set.applyTo(constraintLayout);
                                set.clone(constraintLayout);
                                set.connect(TextViewRCV.getId(), ConstraintSet.TOP, ConstraintSet.PARENT_ID, ConstraintSet.TOP);
                                set.connect(TextViewRCV.getId(), ConstraintSet.BOTTOM, ConstraintSet.PARENT_ID, ConstraintSet.BOTTOM);
                                set.applyTo(constraintLayout);
                            }
                        }
                    }
                    TextViewRCV.setTextSize(TypedValue.COMPLEX_UNIT_SP, 100);
                    TextViewRCV.setText("Producto no existe");
                    valorHablado = "Producto no existe";
                }
                if (!valorHablado.equals("") && datos.getVozPrecio().intValue() == 1) {

                    tts = new TextToSpeech(MainActivity.this, new TextToSpeech.OnInitListener() {
                        @Override
                        public void onInit(int status) {
                            if (status == TextToSpeech.SUCCESS) {
                                int result = tts.setLanguage(spanish);

                                if (result == TextToSpeech.LANG_MISSING_DATA ||
                                        result == TextToSpeech.LANG_NOT_SUPPORTED) {
                                } else {
                                    if (valorHablado == null || "".equals(valorHablado)) {

                                    } else {
                                        tts.speak(valorHablado, TextToSpeech.QUEUE_ADD, null);
                                    }
                                }

                            }
                        }
                    });
                    //tts.shutdown();
                }
                if (!valorHablado.equals("Producto no existe")) {
                    if (datos.getConsultaStock().intValue() == 1) {
                        if (!testSOAP.equals("error") ) {
                            DocumentBuilder newDocumentBuilder = null;
                            try {
                                newDocumentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
                                Document parse = newDocumentBuilder.parse(new ByteArrayInputStream(testSOAP.getBytes()));
                                TreeMap<String, Map> map = new TreeMap<String, Map>();
                                //Map<String, Map> map = new LinkedHashMap<String, Map>();
                                Map<String, String> mapValue = new HashMap<String, String>();
                                NodeList stockComplete = parse.getElementsByTagName("cod_resp");
                                NodeList stockList = parse.getElementsByTagName("stock");
                                String stock_act = "0";
                                Integer stock_actNumber = 0;
                                Integer devcontadorStock = 0;
                                Integer contadorStock = 0;
                                for (int stocks = 0; stocks < stockList.getLength(); stocks++) {
                                    devcontadorStock = 1;
                                    mapValue = new LinkedHashMap<String, String>();
                                    Element stockElement = (Element) stockList.item(stocks);
                                    stock_act = stockElement.getElementsByTagName("stock_act").item(0).getTextContent();
                                    try {
                                        stock_actNumber = Integer.parseInt(stock_act.toString());
                                    } catch (NumberFormatException nfe) {
                                        stock_actNumber = 0;
                                    }
                                    if (stock_actNumber.intValue() > 0 && stock_actNumber.intValue() > datos.getStockMinimo() || datos.getStockMinimo() == 0 && stock_actNumber.intValue() >= 0) {
                                        try {
                                            contadorStock = contadorStock + 1;
                                            codigo = stockElement.getElementsByTagName("sku").item(0).getTextContent();
                                            looper = stockElement.getElementsByTagName("talla").item(0).getTextContent();
                                            color = stockElement.getElementsByTagName("color").item(0).getTextContent();
                                            if (looper.length() > 0) {
                                                if (looper.substring(0, 2).equals("N°")) {
                                                    //Log.i("subs",looper.substring(2));
                                                    if (!looper.substring(2).matches("^([0-9 \\/., ]+)$")) {
                                                        //Log.i("entro","no cumple");
                                                        looper = "TE";
                                                    }
                                                } else {
                                                    looper = "TE";
                                                }

                                                if (map.containsKey(looper.toString())) {
                                                    mapValue = map.get(looper.toString());
                                                    if (mapValue.containsKey("sku")) {
                                                        codigo = codigo + "," + mapValue.get("sku").toString();
                                                    }
                                                    if (mapValue.containsKey("color")) {
                                                        color = color + "," + mapValue.get("color").toString();
                                                    }
                                                }
                                                mapValue.put("color", color);
                                                mapValue.put("sku", codigo);
                                                map.put(looper.toString(), mapValue);
                                                //map.put(looper.toString(), mapValue);
                                            }
                                        } catch (Exception e) {
                                            Log.e("errorstock",String.valueOf(e));
                                        }
                                    }
                                }

                                /*
                                List sortedKeys=new ArrayList(map.keySet());
                                Collections.sort(sortedKeys);
                                Log.e("sorted",String.valueOf(sortedKeys));
                                */
                                /*
                                SortedSet<String> keys = new TreeSet<>(map.keySet());
                                for (String key : keys) {
                                    String value = map.get(key);
                                    // do something
                                }
                                */
                                if (contadorStock.intValue() > 0) {
                                    textViewMSGStock.setText("Producto disponible en las siguientes tallas");
                                    if (datos.getStockGrilla() != 1) {
                                        textViewEND.setTextSize(TypedValue.COMPLEX_UNIT_SP, 40);
                                        textViewEND.setText("Presiona tu número");
                                    }
                                    if (Integer.valueOf(MainActivity.this.getResources().getConfiguration().orientation) == 2) {
                                        //Log.e("error","entro a orientacion 2");
                                        ConstraintSet set = new ConstraintSet();
                                        set.clone(constraintLayout);
                                        set.clear(TextViewRCV.getId(), ConstraintSet.BOTTOM);
                                        set.clear(TextViewRCV.getId(), ConstraintSet.TOP);
                                        set.applyTo(constraintLayout);
                                        set.clone(constraintLayout);
                                        set.connect(TextViewRCV.getId(), ConstraintSet.TOP, buttonConn.getId(), ConstraintSet.BOTTOM);
                                        set.applyTo(constraintLayout);
                                    }
                                    if (datos.getStockGrilla() != 1) {
                                        if (((LinearLayout) ly).getChildCount() > 0)
                                            ((LinearLayout) ly).removeAllViews();

                                        if (((LinearLayout) ly2).getChildCount() > 0)
                                            ((LinearLayout) ly2).removeAllViews();

                                        Iterator myVeryOwnIterator = map.keySet().iterator();
                                        Integer banderaTalla = 0;
                                        Integer banderaTallaTE = 0;
                                        Integer tallaFound;
                                        while (myVeryOwnIterator.hasNext()) {

                                            tallaFound = 0;
                                            String key = (String) myVeryOwnIterator.next();
                                            //String value=(String)meMap.get(key);
                                            LinkedHashMap value = (LinkedHashMap) map.get(key);
                                            ImageButton imagebutton = new ImageButton(MainActivity.this);

                                            if (key.equals("N°6")) {

                                                imagebutton.setImageResource(R.drawable.rsz_paris_logo_main_6_btn);
                                                tallaFound = 1;
                                            }

                                            if (key.equals("N°11")) {

                                                imagebutton.setImageResource(R.drawable.rsz_paris_logo_main_11_btn);
                                                tallaFound = 1;
                                            }
                                            if (key.equals("N°15")) {

                                                imagebutton.setImageResource(R.drawable.rsz_paris_logo_main_15_bt);
                                                tallaFound = 1;
                                            }
                                            if (key.equals("N°16")) {
                                                imagebutton.setImageResource(R.drawable.rsz_paris_logo_main_16_bt);
                                                tallaFound = 1;
                                            }
                                            if (key.equals("N°17")) {
                                                imagebutton.setImageResource(R.drawable.rsz_1paris_logo_main_17_bt);
                                                tallaFound = 1;
                                            }
                                            if (key.equals("N°18")) {
                                                imagebutton.setImageResource(R.drawable.rsz_paris_logo_main_18_bt);
                                                tallaFound = 1;
                                            }
                                            if (key.equals("N°19")) {
                                                imagebutton.setImageResource(R.drawable.rsz_paris_logo_main_19_bt);
                                                tallaFound = 1;
                                            }
                                            if (key.equals("N°20")) {
                                                imagebutton.setImageResource(R.drawable.rsz_paris_logo_main_20_bt);
                                                tallaFound = 1;
                                            }
                                            if (key.equals("N°21")) {
                                                imagebutton.setImageResource(R.drawable.rsz_paris_logo_main_21_bt);
                                                tallaFound = 1;
                                            }
                                            if (key.equals("N°22")) {
                                                imagebutton.setImageResource(R.drawable.rsz_paris_logo_main_22_bt);
                                                tallaFound = 1;
                                            }
                                            if (key.equals("N°23")) {
                                                imagebutton.setImageResource(R.drawable.rsz_paris_logo_main_23_bt);
                                                tallaFound = 1;
                                            }
                                            if (key.equals("N°24")) {
                                                imagebutton.setImageResource(R.drawable.rsz_paris_logo_main_24_bt);
                                                tallaFound = 1;
                                            }
                                            if (key.equals("N°25")) {
                                                imagebutton.setImageResource(R.drawable.rsz_paris_logo_main_25_bt);
                                                tallaFound = 1;
                                            }
                                            if (key.equals("N°26")) {
                                                imagebutton.setImageResource(R.drawable.rsz_paris_logo_main_26_bt);
                                                tallaFound = 1;
                                            }
                                            if (key.equals("N°27")) {
                                                imagebutton.setImageResource(R.drawable.rsz_paris_logo_main_27_bt);
                                                tallaFound = 1;
                                            }
                                            if (key.equals("N°28")) {
                                                imagebutton.setImageResource(R.drawable.rsz_paris_logo_main_28_bt);
                                                tallaFound = 1;
                                            }
                                            if (key.equals("N°29")) {
                                                imagebutton.setImageResource(R.drawable.rsz_paris_logo_main_29_bt);
                                                tallaFound = 1;
                                            }
                                            if (key.equals("N°30")) {
                                                imagebutton.setImageResource(R.drawable.rsz_paris_logo_main_30_bt);
                                                tallaFound = 1;
                                            }
                                            if (key.equals("N°31")) {
                                                imagebutton.setImageResource(R.drawable.rsz_paris_logo_main_31_bt);
                                                tallaFound = 1;
                                            }
                                            if (key.equals("N°32")) {
                                                imagebutton.setImageResource(R.drawable.rsz_paris_logo_main_32_bt);
                                                tallaFound = 1;
                                            }
                                            if (key.equals("N°33")) {
                                                imagebutton.setImageResource(R.drawable.rsz_paris_logo_main_33_bt);
                                                tallaFound = 1;
                                            }
                                            if (key.equals("N°34")) {
                                                imagebutton.setImageResource(R.drawable.rsz_paris_logo_main_34_bt);
                                                tallaFound = 1;
                                            }
                                            if (key.equals("N°35")) {
                                                imagebutton.setImageResource(R.drawable.rsz_paris_logo_main_35_bt);
                                                tallaFound = 1;
                                            }
                                            if (key.equals("N°35 1/2") || key.equals("N°35.5") || key.equals("N°35,5")) {
                                                imagebutton.setImageResource(R.drawable.rsz_paris_logo_main_35_5_bt);
                                                tallaFound = 1;
                                            }
                                            if (key.equals("N°36")) {
                                                imagebutton.setImageResource(R.drawable.rsz_paris_logo_main_36_bt);
                                                tallaFound = 1;
                                            }
                                            if (key.equals("N°36 1/2") || key.equals("N°36.5") || key.equals("N°36,5")) {
                                                imagebutton.setImageResource(R.drawable.rsz_1paris_logo_main_36_5_bt);
                                                tallaFound = 1;
                                            }
                                            if (key.equals("N°37")) {
                                                imagebutton.setImageResource(R.drawable.rsz_paris_logo_main_37_bt);
                                                tallaFound = 1;
                                            }
                                            if (key.equals("N°37 1/2") || key.equals("N°37.5") || key.equals("N°37,5")) {
                                                imagebutton.setImageResource(R.drawable.rsz_1paris_logo_main_37_5_bt);
                                                tallaFound = 1;
                                            }
                                            if (key.equals("N°38")) {
                                                imagebutton.setImageResource(R.drawable.rsz_paris_logo_main_38_bt);
                                                tallaFound = 1;
                                            }
                                            if (key.equals("N°38 1/2") || key.equals("N°38.5") || key.equals("N°38,5")) {
                                                imagebutton.setImageResource(R.drawable.rsz_paris_logo_main_38_5_bt);
                                                tallaFound = 1;
                                            }
                                            if (key.equals("N°39")) {
                                                imagebutton.setImageResource(R.drawable.rsz_paris_logo_main_39_bt);
                                                tallaFound = 1;
                                            }
                                            if (key.equals("N°39 1/2") || key.equals("N°39.5") || key.equals("N°39,5")) {
                                                imagebutton.setImageResource(R.drawable.rsz_paris_logo_main_39_5_bt);
                                                tallaFound = 1;
                                            }
                                            if (key.equals("N°40")) {
                                                imagebutton.setImageResource(R.drawable.rsz_paris_logo_main_40_bt);
                                                tallaFound = 1;
                                            }
                                            if (key.equals("N°40 1/2") || key.equals("N°40.5") || key.equals("N°40,5")) {
                                                imagebutton.setImageResource(R.drawable.rsz_paris_logo_main_40_5_bt);
                                                tallaFound = 1;
                                            }
                                            if (key.equals("N°41")) {
                                                imagebutton.setImageResource(R.drawable.rsz_paris_logo_main_41_bt);
                                                tallaFound = 1;
                                            }
                                            if (key.equals("N°41 1/2") || key.equals("N°41.5") || key.equals("N°41,5")) {
                                                imagebutton.setImageResource(R.drawable.rsz_paris_logo_main_41_5_bt);
                                                tallaFound = 1;
                                            }
                                            if (key.equals("N°42")) {
                                                imagebutton.setImageResource(R.drawable.rsz_paris_logo_main_42_bt);
                                                tallaFound = 1;
                                            }
                                            if (key.equals("N°42 1/2") || key.equals("N°42.5") || key.equals("N°42,5")) {
                                                imagebutton.setImageResource(R.drawable.rsz_paris_logo_main_42_5_bt);
                                                tallaFound = 1;
                                            }
                                            if (key.equals("N°43")) {
                                                imagebutton.setImageResource(R.drawable.rsz_paris_logo_main_43_bt);
                                                tallaFound = 1;
                                            }
                                            if (key.equals("N°43 1/2") || key.equals("N°43.5") || key.equals("N°43,5")) {
                                                imagebutton.setImageResource(R.drawable.rsz_paris_logo_main_43_5_bt);
                                                tallaFound = 1;
                                            }
                                            if (key.equals("N°44")) {
                                                imagebutton.setImageResource(R.drawable.rsz_paris_logo_main_44_bt);
                                                tallaFound = 1;
                                            }
                                            if (key.equals("N°44 1/2") || key.equals("N°44.5") || key.equals("N°44,5")) {
                                                imagebutton.setImageResource(R.drawable.rsz_paris_logo_main_44_5_bt);
                                                tallaFound = 1;
                                            }
                                            if (key.equals("N°45")) {
                                                imagebutton.setImageResource(R.drawable.rsz_paris_logo_main_45_bt);
                                                tallaFound = 1;
                                            }
                                            if (key.equals("N°46")) {
                                                imagebutton.setImageResource(R.drawable.rsz_paris_logo_main_46_bt);
                                                tallaFound = 1;
                                            }
                                            if (key.equals("N°47")) {
                                                imagebutton.setImageResource(R.drawable.rsz_paris_logo_main_47_bt);
                                                tallaFound = 1;
                                            }
                                            if (key.equals("TE")) {
                                                banderaTallaTE = 1;
                                                imagebutton.setImageResource(R.drawable.rsz_te_bt);
                                                tallaFound = 1;
                                            }
                                            if (tallaFound == 1) {
                                                banderaTalla += 1;
                                                imagebutton.setOnClickListener(getOnClickDoSomething(imagebutton, value));
                                                imagebutton.setBackground(null);
                                                //ly.addView(imagebutton);
                                                if (banderaTalla.intValue() > 5) {
                                                    ly2.addView(imagebutton);
                                                } else {
                                                    ly.addView(imagebutton);
                                                }
                                                if (banderaTalla.intValue() == 10) {
                                                    break;
                                                }
                                            }
                                            //setText(TextViewRCV,key);
                                        }
                                        if (banderaTallaTE.intValue() == 1) {
                                            textViewMSGStock.setText("Producto disponible en las siguientes tallas (TE = Talla Estándar)");
                                        }
                            /*
                            constraintLayout.addView(ly);
                            if (banderaTalla.intValue() > 5) {
                                constraintLayout.addView(ly2);
                            }
                            */
                                    } else { //else para grilla
                                        //contadorStock = 0;
                                        if (contadorStock > 0) {
                                            textViewBanderaGreen.setVisibility(View.VISIBLE);
                                            textViewBanderaGreen.setBackgroundColor(Color.parseColor("#008000"));
                                            linearLayoutTabla.setVisibility(View.VISIBLE);
                                            layoutTabla.setVisibility(View.VISIBLE);
                                            Tabla tabla = new Tabla(MainActivity.this, (TableLayout) findViewById(R.id.tabla));
                                            TableLayout tablaLayout = findViewById(R.id.tabla);
                                            tablaLayout.removeAllViews();
                                            TableLayout tableLayoutHeader = findViewById(R.id.HEaderID2);
                                            tableLayoutHeader.removeAllViews();

                                            Tabla tablaHeader = new Tabla(MainActivity.this, (TableLayout) findViewById(R.id.HEaderID2));
                                            tablaHeader.agregarCabecera(R.array.cabecera_tabla, 0);
                                            TreeMap<String, String> mapTalla = new TreeMap<String, String>();
                                            TreeMap<String, String> mapColor = new TreeMap<String, String>();
                                            TreeMap<String, String> mapSKU = new TreeMap<String, String>();

                                            //scrollvertical.setScrollBarStyle(Style.);
                                            tabla.agregarCabecera(R.array.cabecera_tabla, 1);
                                            for (int stocks = 0; stocks < stockList.getLength(); stocks++) {
                                                ArrayList<String> elementos = new ArrayList<String>();
                                                Element stockElement = (Element) stockList.item(stocks);
                                                stock_act = stockElement.getElementsByTagName("stock_act").item(0).getTextContent();
                                                try {
                                                    stock_actNumber = Integer.parseInt(stock_act.toString());
                                                } catch (NumberFormatException nfe) {
                                                    stock_actNumber = 0;
                                                }
                                                if (stock_actNumber.intValue() > 0 && stock_actNumber.intValue() > datos.getStockMinimo() || datos.getStockMinimo() == 0 && stock_actNumber.intValue() >= 0) {
                                                    try {
                                                        codigo = stockElement.getElementsByTagName("sku").item(0).getTextContent();
                                                        looper = stockElement.getElementsByTagName("talla").item(0).getTextContent();
                                                        color = stockElement.getElementsByTagName("color").item(0).getTextContent();

                                                        if (mapTalla.containsKey(looper)) {
                                                            mapTalla.put(looper, mapTalla.get(looper).toString() + "," + looper);
                                                        } else {
                                                            mapTalla.put(looper, looper);
                                                        }

                                                        if (mapColor.containsKey(looper)) {
                                                            mapColor.put(looper, mapColor.get(looper).toString() + "," + color);
                                                        } else {
                                                            mapColor.put(looper, color);
                                                        }

                                                        if (mapSKU.containsKey(looper)) {
                                                            mapSKU.put(looper, mapSKU.get(looper).toString() + "," + codigo);
                                                        } else {
                                                            mapSKU.put(looper, codigo);
                                                        }

                                                        //mapColor.put(looper,color);
                                                        //mapSKU.put(looper,codigo);
                                                        //stock = stockElement.getElementsByTagName("stock_act").item(0).getTextContent();

                                                        //elementos.add(String.valueOf(looper));
                                                        //elementos.add(String.valueOf(color));
                                                        //elementos.add(String.valueOf(codigo));

                                                        //tabla.agregarFilaTabla(elementos, 0);
                                                        //tablaHeader.agregarFilaTabla(elementos, 1);
                                                    } catch (Exception e) {
                                                        Log.e("errorgrilla", String.valueOf(e));
                                                    }
                                                }
                                            }


                                            Iterator myVeryOwnIterator2 = mapTalla.keySet().iterator();
                                            while (myVeryOwnIterator2.hasNext()) {
                                                String[] stallas;
                                                String[] scolors;
                                                String[] scodigos;
                                                String key = (String) myVeryOwnIterator2.next();
                                                //String value=(String)meMap.get(key);
                                                String valueTalla = (String) mapTalla.get(key);
                                                String valueColors = (String) mapColor.get(key);
                                                String valueCodigo = (String) mapSKU.get(key);
                                                stallas = valueTalla.split(",");
                                                scolors = valueColors.split(",");
                                                scodigos = valueCodigo.split(",");
                                                for (i=0;i<stallas.length;i++) {
                                                    ArrayList<String> elementos = new ArrayList<String>();
                                                    elementos.add(String.valueOf(stallas[i]));
                                                    elementos.add(String.valueOf(scolors[i]));
                                                    elementos.add(String.valueOf(scodigos[i]));
                                                    tabla.agregarFilaTabla(elementos, 0);
                                                    tablaHeader.agregarFilaTabla(elementos, 1);
                                                }
                                               // Log.e("talla",valueTalla);
                                            }

                                            //Log.e("skumapsort",String.valueOf(mapTalla));
                                        } else {
                                            if (Integer.valueOf(MainActivity.this.getResources().getConfiguration().orientation) == 2) {
                                                ConstraintSet set = new ConstraintSet();
                                                // reagrupados la vista ya que no tiene stock
                                                set.clone(constraintLayout);
                                                set.clear(TextViewRCV.getId(), ConstraintSet.TOP);
                                                //set.clear(TextViewRCV.getId(),ConstraintSet.TOP);
                                                set.applyTo(constraintLayout);
                                                set.clone(constraintLayout);
                                                set.connect(TextViewRCV.getId(), ConstraintSet.TOP, ConstraintSet.PARENT_ID, ConstraintSet.TOP);
                                                set.connect(TextViewRCV.getId(), ConstraintSet.BOTTOM, ConstraintSet.PARENT_ID, ConstraintSet.BOTTOM);
                                                set.applyTo(constraintLayout);
                                            }
                                        }
                                    }
                                }  else {
                                    textViewBandera.setVisibility(View.VISIBLE);
                                    textViewBandera.setBackgroundColor(Color.parseColor("#FF0000"));
                                    if (devcontadorStock == 0) {
                                        textViewEND.setText("Sin información de Stock");
                                    } else {
                                        textViewEND.setText("Producto sin stock en tienda");
                                    }
                                }
                            } catch (ParserConfigurationException e) {
                                e.printStackTrace();
                            } catch (SAXException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        } else {
                            //Log.e("error", "entro al final");
                            textViewBandera.setVisibility(View.VISIBLE);
                            textViewBandera.setBackgroundColor(Color.parseColor("#FF0000"));
                            textViewEND.setText("Sin información de Stock");
                            if (Integer.valueOf(MainActivity.this.getResources().getConfiguration().orientation) == 2) {
                                ConstraintSet set = new ConstraintSet();
                                // reagrupados la vista ya que no tiene stock
                                set.clone(constraintLayout);
                                set.clear(TextViewRCV.getId(), ConstraintSet.TOP);
                                //set.clear(TextViewRCV.getId(),ConstraintSet.TOP);
                                set.applyTo(constraintLayout);
                                set.clone(constraintLayout);
                                set.connect(TextViewRCV.getId(), ConstraintSet.TOP, ConstraintSet.PARENT_ID, ConstraintSet.TOP);
                                set.connect(TextViewRCV.getId(), ConstraintSet.BOTTOM, ConstraintSet.PARENT_ID, ConstraintSet.BOTTOM);
                                set.applyTo(constraintLayout);
                            }

                        }
                    }
                }

            }
            returnWelcome();
        }
    }

    View.OnClickListener getOnClickDoSomething(final ImageButton button,  final LinkedHashMap Mapeo)  {
        return new View.OnClickListener() {
            public void onClick(View v) {

                    ConstraintLayout constraintLayout = findViewById(R.id.constraintLayout);
                    //LinearLayout ly =  findViewById(R.id.layout1y);

                    if (((LinearLayout) ly).getChildCount() > 0)
                        ((LinearLayout) ly).removeAllViews();
                    if (((LinearLayout) ly2).getChildCount() > 0)
                        ((LinearLayout) ly2).removeAllViews();
                    Integer i;

                    if (ly.getParent() != null)
                        ((ViewGroup) ly.getParent()).removeView(ly);
                /*
                if(ly2.getParent()!=null)
                    ((ViewGroup)ly2.getParent()).removeView(ly2);
                */
                    sku = (String) Mapeo.get("sku");
                    colore = (String) Mapeo.get("color");
                    skus = sku.split(",");
                    colors = colore.split(",");

                    ly.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL);


                    LinearLayout lyv1 = new LinearLayout(MainActivity.this);
                    lyv1.setGravity(Gravity.CENTER_VERTICAL);
                    lyv1.setOrientation(LinearLayout.VERTICAL);
                    //lyv1.setHorizontalGravity(LinearLayout.TEXT_ALIGNMENT_CENTER);
                    lyv1.setPadding(0, 0, 50, 0);

                    lyv1.addView(button);
                    ly.addView(lyv1);

                    //constraintLayout.addView(ly);
                    for (i = 0; i < skus.length; i++) {
                        //Log.e("skumapeo",skus[i]);
                        LinearLayout lyv = new LinearLayout(MainActivity.this);
                        lyv.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL);
                        lyv.setOrientation(LinearLayout.VERTICAL);
                        lyv.setHorizontalGravity(LinearLayout.TEXT_ALIGNMENT_CENTER);
                        lyv.setPadding(0, 0, 50, 0);
                        TextView textViewSKU = new TextView(MainActivity.this);

                        TextView textViewCOLOR = new TextView(MainActivity.this);
                        textViewCOLOR.setTextAlignment(LinearLayout.TEXT_ALIGNMENT_CENTER);
                        textViewCOLOR.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
                        textViewCOLOR.setTextColor(Color.parseColor("#FFFFFF"));
                        textViewCOLOR.setText(colors[i]);

                        ImageView checkok = new ImageView(MainActivity.this);
                        checkok.setImageResource(R.drawable.rsz_ok_white);

                        textViewSKU.setTextAlignment(LinearLayout.TEXT_ALIGNMENT_CENTER);
                        textViewSKU.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
                        textViewSKU.setTextColor(Color.parseColor("#FFFFFF"));
                        textViewSKU.setText(skus[i]);


                        lyv.addView(textViewCOLOR);
                        lyv.addView(checkok);
                        lyv.addView(textViewSKU);
                        ly.addView(lyv);
                    }
                    textViewMSGStock.setText("Producto disponible en los siguientes colores");
                    textViewEND.setVisibility(View.INVISIBLE);
                    constraintLayout.addView(ly);

            }
        };
    }

}
