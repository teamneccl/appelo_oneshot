package com.example.migue.appelo;


import android.app.Application;
import android.content.Context;

public class AppContext extends Application {
    public static AppContext instance;
    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }
    @Override
    public Context getApplicationContext() {
        return super.getApplicationContext();
    }
    public static AppContext getInstance() {
        return instance;
    }
}