package com.example.migue.appelo.DB;

/**
 * Created by macevedo on 10-01-2018.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

//import com.example.migue.appelo.Model.Configuration;

import java.util.ArrayList;
import java.util.List;


import com.example.migue.appelo.Model.Logs;

public class LogOperations {
    public static final String LOGTAG = "LOG_MNGMNT_SYS";
    public Cursor cursor;
    SQLiteOpenHelper dbhandler;
    SQLiteDatabase database;

    private static final String[] allColumns = {
            ConfigurationDBHandler.COLUMN_ID,
            ConfigurationDBHandler.COLUMN_LOGTYPE,
            ConfigurationDBHandler.COLUMN_INPUT,
            ConfigurationDBHandler.COLUMN_OUTPUT,
            ConfigurationDBHandler.COLUMN_DATE

    };

    public LogOperations(Context context){
        dbhandler = new ConfigurationDBHandler(context);
    }

    public void open(){
        //Log.i(LOGTAG,"Log Database Opened");
        database = dbhandler.getWritableDatabase();


    }
    public void close(){
        //Log.i(LOGTAG, "Log Database Closed");
        dbhandler.close();
    }

    public void removeLogs(String fecha) {
        //Log.i("llego", fecha);
        //database.delete(ConfigurationDBHandler.TABLE_LOGS, ConfigurationDBHandler.COLUMN_DATE + "=" + logs.getLogId(), null);
        // where date(logdate) <= date("+fecha+");
        database.execSQL("delete from logs where date(logdate) <= date(\""+fecha+"\")");
        database.close();
    }

    public Logs addLog(Logs logs){
        ContentValues values  = new ContentValues();
        //values.put(LogDBHandler.COLUMN_ID, logs.getLogId());
        values.put(ConfigurationDBHandler.COLUMN_LOGTYPE, logs.getLogType());
        values.put(ConfigurationDBHandler.COLUMN_INPUT, logs.getInputDesc());

        values.put(ConfigurationDBHandler.COLUMN_OUTPUT, logs.getOutputDesc());
        values.put(ConfigurationDBHandler.COLUMN_DATE, logs.getLogDate());


        long insertid = database.insert(ConfigurationDBHandler.TABLE_LOGS,null,values);
        logs.setLogId(insertid);
        database.close();
        //Log.i("TAGI","entro al add " + String.valueOf(insertid));
        return logs;

    }

    // Getting single Configuration
    public Logs getLog(long id) {
        Logs e = new Logs();
        cursor = database.query(ConfigurationDBHandler.TABLE_LOGS,allColumns,ConfigurationDBHandler.COLUMN_ID + "=?",new String[]{String.valueOf(id)},null,null, null, null);
        try {
            if (cursor.getCount() > 0)
                cursor.moveToFirst();
            else
                return null;
            if(cursor.getCount() > 0) {
                e = new Logs(Long.parseLong(cursor.getString(0)), cursor.getString(1), cursor.getString(2), cursor.getString(3), personalCast(cursor.getString(4)));
            }
        } finally {
            cursor.close();
        }


        // return Configuration
        return e;
    }



    private String personalCast(String numero){
        String number = "0";
        Integer revision = 0;
        try
        {       revision = Integer.valueOf(numero);
            return numero;
        }
        catch (NumberFormatException e)
        {
            return "0";
        }

    }


    public List<Logs> getAllLogs() {
        List<Logs> logsList = new ArrayList<>();
        cursor = database.query(ConfigurationDBHandler.TABLE_LOGS,allColumns,null,null,null, null, null);
        try {

            if(cursor.getCount() > 0){
                while(cursor.moveToNext()){
                    Logs logs = new Logs();
                    logs.setLogId(cursor.getLong(cursor.getColumnIndex(ConfigurationDBHandler.COLUMN_ID)));
                    logs.setLogType(cursor.getString(cursor.getColumnIndex(ConfigurationDBHandler.COLUMN_LOGTYPE)));
                    logs.setInputDesc(cursor.getString(cursor.getColumnIndex(ConfigurationDBHandler.COLUMN_INPUT)));
                    logs.setOutputDesc(cursor.getString(cursor.getColumnIndex(ConfigurationDBHandler.COLUMN_OUTPUT)));

                    logs.setLogDate(cursor.getString(cursor.getColumnIndex(ConfigurationDBHandler.COLUMN_DATE)));
                    logsList.add(logs);
                }
            }
        } finally {
            cursor.close();
        }


        // return All Configuration
        return logsList;
    }

}

