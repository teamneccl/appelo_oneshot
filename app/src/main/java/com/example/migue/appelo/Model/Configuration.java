package com.example.migue.appelo.Model;

import android.util.Log;

/**
 * Created by migue on 16/12/2017.
 */

public class Configuration {
    private long empId,Consultor;
    private String IPDesc,NombreLocal,UrlWs;
    private String PORTDesc;
    private String Tipo,Local;
    private String AbrevLocal;
    private Integer TprecioPantalla,Skin,ConsultaStock,StockMinimo,Status,timeSocket, timeWS,logActivo,logNroDias,contrasenaAdmin;
    private Integer muestraPublicidad, cantImagenes, tiempoImagen,tiempoInactividad,stockGrilla, rebootHour, rebootMinute, vozPrecio;


    public Integer getMuestraPublicidad() {
        return muestraPublicidad;
    }

    public void setMuestraPublicidad(Integer muestraPublicidad) {
        this.muestraPublicidad = muestraPublicidad;
    }

    public Integer getCantImagenes() {
        return cantImagenes;
    }

    public void setCantImagenes(Integer cantImagenes) {
        this.cantImagenes = cantImagenes;
    }

    public Integer getTiempoImagen() {
        return tiempoImagen;
    }

    public void setTiempoImagen(Integer tiempoImagen) {
        this.tiempoImagen = tiempoImagen;
    }

    public Integer getTiempoInactividad() {
        return tiempoInactividad;
    }

    public void setTiempoInactividad(Integer tiempoInactividad) {
        this.tiempoInactividad = tiempoInactividad;
    }

    public Integer getTimeSocket() {
        return timeSocket;
    }

    public void setTimeSocket(Integer timeSocket) {
        this.timeSocket = timeSocket;
    }

    public Integer getTimeWS() {
        return timeWS;
    }

    public void setTimeWS(Integer timeWS) {
        this.timeWS = timeWS;
    }

    public Integer getLogActivo() {
        return logActivo;
    }

    public void setLogActivo(Integer logActivo) {
        this.logActivo = logActivo;
    }

    public Integer getLogNroDias() {
        return logNroDias;
    }

    public void setLogNroDias(Integer logNroDias) {
        this.logNroDias = logNroDias;
    }

    public Integer getStockGrilla() {
        return stockGrilla;
    }

    public void setStockGrilla(Integer stockGrilla) {
        this.stockGrilla = stockGrilla;
    }

    public Integer getRebootHour() {
        return rebootHour;
    }

    public void setRebootHour(Integer rebootHour) {
        this.rebootHour = rebootHour;
    }

    public Integer getRebootMinute() {
        return rebootMinute;
    }

    public void setRebootMinute(Integer rebootMinute) {
        this.rebootMinute = rebootMinute;
    }

    public Integer getVozPrecio() {
        return vozPrecio;
    }

    public void setVozPrecio(Integer vozPrecio) {
        this.vozPrecio = vozPrecio;
    }

    public Integer getContrasenaAdmin() {
        return contrasenaAdmin;
    }

    public void setContrasenaAdmin(Integer contrasenaAdmin) {
        this.contrasenaAdmin = contrasenaAdmin;
    }

    public Configuration(long empId, String IPDesc, String PORTDesc, String Tipo, String Local, long Consultor, String NombreLocal, String UrlWs, Integer TprecioPantalla, Integer Skin, Integer ConsultaStock, Integer StockMinimo, Integer Status, String AbrevLocal, Integer muestraPublicidad, Integer cantImagenes, Integer tiempoImagen, Integer tiempoInactividad, Integer timeSocket, Integer timeWS, Integer logActivo, Integer logNroDias, Integer stockGrilla, Integer rebootHour, Integer rebootMinute, Integer vozPrecio, Integer contrasenaAdmin){
        this.empId = empId;
        this.IPDesc = IPDesc;
        this.PORTDesc = PORTDesc;
        this.Tipo = Tipo;
        this.Local = Local;
        this.Consultor = Consultor;
        this.NombreLocal = NombreLocal;
        this.UrlWs = UrlWs;
        this.TprecioPantalla = TprecioPantalla;
        this.Skin = Skin;
        this.ConsultaStock = ConsultaStock;
        this.StockMinimo = StockMinimo;
        this.Status = Status;
        this.AbrevLocal = AbrevLocal;
        this.muestraPublicidad = muestraPublicidad;
        this.cantImagenes = cantImagenes;
        this.tiempoImagen = tiempoImagen;
        this.tiempoInactividad = tiempoInactividad;
        this.timeSocket = timeSocket;
        this.timeWS = timeWS;
        this.logActivo = logActivo;
        this.logNroDias = logNroDias;
        this.stockGrilla = stockGrilla;
        this.rebootHour = rebootHour;
        this.rebootMinute = rebootMinute;
        this.vozPrecio = vozPrecio;
        this.contrasenaAdmin = contrasenaAdmin;



        //Log.i("socket",String.valueOf(timeSocket));


    }

    public Configuration(){

    }

    public long getEmpId() {
        return empId;
    }

    public void setEmpId(long empId) {
        this.empId = empId;
    }

    public String getIPDesc() {
        return IPDesc;
    }

    public void setIPDesc(String ipDesc) {
        this.IPDesc = ipDesc;
    }

    public String getPORTDesc() {
        return PORTDesc;
    }

    public void setPORTDesc(String portDesc) {
        this.PORTDesc = portDesc;
    }

    public String getTipo() {
        return Tipo;
    }

    public void setTipo(String tipo) {
        this.Tipo = tipo;
    }

    /* NUEVOS GETTER AND SETTER */

    public String getLocal() {
        return Local;
    }

    public void setLocal(String local) {
        this.Local = local;
    }

    public Long getConsultor() {
        return Consultor;
    }

    public void setConsultor(Long consultor) {
        this.Consultor = consultor;
    }

    public String getNombreLocal() {
        return NombreLocal;
    }

    public void setNombreLocal(String nombreLocal) {
        this.NombreLocal = nombreLocal;
    }

    public String getUrlWs() {
        return UrlWs;
    }

    public void setUrlWs(String urlWs) {
        this.UrlWs = urlWs;
    }

    public Integer getTprecioPantalla() {
        return TprecioPantalla;
    }

    public void setTprecioPantalla(Integer tprecioPantalla) {
        this.TprecioPantalla = tprecioPantalla;
    }

    public Integer getSkin() {
        return Skin;
    }

    public void setSkin(Integer skin) {
        this.Skin = skin;
    }

    public Integer getConsultaStock() {
        return ConsultaStock;
    }

    public void setConsultaStock(Integer consultaStock) {
        this.ConsultaStock = consultaStock;
    }

    public Integer getStockMinimo() {
        return StockMinimo;
    }

    public void setStockMinimo(Integer stockMinimo) {
        this.StockMinimo = stockMinimo;
    }

    public Integer getStatus() {
        return Status;
    }

    public void setStatus(Integer status) {
        this.Status = status;
    }

    public String getAbrevLocal() {
        return AbrevLocal;
    }

    public void setAbrevLocal(String abrevLocal) {
        this.AbrevLocal = abrevLocal;
    }

    public String toString(){
        return "Emp id: "+getEmpId()+ "\n" +
                "IPDesc: "+getIPDesc()  + "\n" +
                "PORTDesc: "+getPORTDesc()  + "\n" +
                "Tipo: "+getTipo() + "\n" +
                "Local: "+getLocal() + "\n" +
                "Consultor: "+getConsultor() + "\n" +
                "UrlWs: "+getUrlWs() + "\n" +
                "NombreLocal: "+getNombreLocal() + "\n" +
                "TprecioPantalla: "+getTprecioPantalla() + "\n" +
                "Skin: "+getSkin() + "\n" +
                "ConsultaStock: "+getConsultaStock() + "\n" +
                "StockMinimo: "+getStockMinimo() + "\n" +
                "Status: "+getStatus() + "\n" +
                "AbrevLocal:" + getAbrevLocal()  + "\n" +
                "muestraPublicidad" + getMuestraPublicidad()  + "\n" +
                "cantImagenes" + getCantImagenes() + "\n" +
                "tiempoImagen" + getTiempoImagen() + "\n" +
                "tiempoinactividad" + getTiempoInactividad() + "\n" +
                "timeSocket: "+ getTimeSocket()  + "\n" +
                "timeWS: "+ getTimeWS() + "\n" +
                "logActivo: "+ getLogActivo()  + "\n" +
                "logNroDias: "+ getLogNroDias() + "\n" +
                "stockGrilla: "+ getStockGrilla() + "\n" +
                "rebootHour:" + getRebootHour() + "\n" +
                "rebootMinute:" + getRebootMinute() + "\n" +
                "vozPrecio:" + getVozPrecio() + "\n" +
                "contrasenaAdmin:" + getContrasenaAdmin();
    }
}
