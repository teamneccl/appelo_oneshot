/*
 *
 *  *    Copyright (C) 2016 Amit Shekhar
 *  *    Copyright (C) 2011 Android Open Source Project
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *        http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */

package com.example.migue.appelo.Server;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.net.Uri;
import android.os.PowerManager;
import android.text.TextUtils;
import android.util.Log;
import android.util.Pair;

import com.example.migue.appelo.Model.Response;
import com.example.migue.appelo.Model.RowDataRequest;
import com.example.migue.appelo.Model.TableDataResponse;
import com.example.migue.appelo.Model.UpdateRowResponse;
import com.example.migue.appelo.utils.Constants;
import com.example.migue.appelo.utils.DatabaseFileProvider;
import com.example.migue.appelo.utils.DatabaseHelper;
import com.example.migue.appelo.utils.PrefHelper;
import com.example.migue.appelo.utils.Utils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;


import net.sqlcipher.database.SQLiteDatabase;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

/**
 * Created by amitshekhar on 06/02/17.
 */

public class RequestHandler {

    private final Context mContext;
    private final Gson mGson;
    private final AssetManager mAssets;
    private boolean isDbOpened;
    private SQLiteDatabase mDatabase;
    private HashMap<String, Pair<File, String>> mDatabaseFiles;
    private HashMap<String, Pair<File, String>> mCustomDatabaseFiles;
    private String mSelectedDatabase = null;
    private Session session;


    public RequestHandler(Context context) {
        mContext = context;
        mAssets = context.getResources().getAssets();
        mGson = new GsonBuilder().serializeNulls().create();
    }

    public void handle(Socket socket) throws IOException {
        BufferedReader reader = null;
        PrintStream output = null;
        try {
            String route = null;
            String search = null;
            String userName = "";
            String passWord = "";
            String temp = "";

            // Read HTTP headers and parse out the route.
            reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));

            String line;

            while (!TextUtils.isEmpty(line = reader.readLine())) {
                //Log.i("reader",String.valueOf(line));
                if (line.startsWith("GET /")) {

                    int start = line.indexOf('/') + 1;
                    int end = line.indexOf(' ', start);
                    route = line.substring(start, end);
                    if (route.startsWith("authorizeLogin")) {
                        temp = "http://www.temp.com/" + route;
                        //Log.i("test",String.valueOf(temp));
                        Uri uri = Uri.parse(temp);
                        String server = uri.getAuthority();
                        String path = uri.getPath();
                        String protocol = uri.getScheme();
                        Set<String> args = uri.getQueryParameterNames();
                        userName = uri.getQueryParameter("username");
                        passWord = uri.getQueryParameter("password");
                        //Log.i("test",String.valueOf(userName));
                        //Log.i("test",String.valueOf(passWord));
                    }
                    break;
                }
            }
            byte[] bytes;

            session = new Session(mContext);
            //session.cleanSession();

            if (route == null || route.isEmpty()) {
                //route = "index.html";
                //Log.i("test2",route);
                route = "login.html";
                /*
                try {
                    Process proc = Runtime.getRuntime().exec("reboot");
                    proc.waitFor();
                } catch (Exception ex) {
                    Log.i("error",""+ex);
                }
                */
            }
            if (route.startsWith("logs")) {
                route = "indexLogs.html";
            }



            // Output stream that we send the response to
            output = new PrintStream(socket.getOutputStream());

            if (route.startsWith("getDbList")) {
                final String response = getDBListResponse();
                bytes = response.getBytes();
            } else if (route.startsWith("authorizeLogin")) {
                if (userName.equals("admin") && passWord.equals("1234")) {
                    final String response = "{\"status\":\"OK\"}";
                    bytes = response.getBytes();
                    session.setSession("OK");
                } else {
                    final String response = "{\"status\":\"NOK\"}";
                    bytes = response.getBytes();
                    session.cleanSession();
                }
            } else if (route.startsWith("getAuthorization")) {
                if (session.getSession().equals("OK")) {
                    final String response = "{\"status\":\"OK\"}";
                    bytes = response.getBytes();
                } else {
                    final String response = "{\"status\":\"NOK\"}";
                    bytes = response.getBytes();
                }
            }else if (route.startsWith("getSignOut")) {
                session.cleanSession();
                final String response = "{\"status\":\"OK\"}";
                bytes = response.getBytes();
            } else if (route.startsWith("getAllDataFromTheTable")) {
                final String response = getAllDataFromTheTableResponse(route);
                bytes = response.getBytes();
            } else if (route.startsWith("getTableList")) {
                final String response = getTableListResponse(route);
                bytes = response.getBytes();
            } else if (route.startsWith("addTableData")) {
                final String response = addTableDataAndGetResponse(route);
                bytes = response.getBytes();
            } else if (route.startsWith("updateTableData")) {
                Log.i("paso",route);
                final String response = updateTableDataAndGetResponse(route);
                bytes = response.getBytes();
            } else if (route.startsWith("deleteTableData")) {
                final String response = deleteTableDataAndGetResponse(route);
                bytes = response.getBytes();
            } else if (route.startsWith("query")) {
                final String response = executeQueryAndGetResponse(route);
                bytes = response.getBytes();
            } else if (route.startsWith("downloadDb")) {
                bytes = Utils.getDatabase(mSelectedDatabase, mDatabaseFiles);
            }   else if (route.startsWith("login")) {
                bytes = Utils.loadContent(route, mAssets);
            } else {
                bytes = Utils.loadContent(route, mAssets);
            }

            if (null == bytes) {
                writeServerError(output);
                return;
            }

            // Send out the content.
            output.println("HTTP/1.0 200 OK");
            output.println("Content-Type: " + Utils.detectMimeType(route));

            if (route.startsWith("downloadDb")) {
                output.println("Content-Disposition: attachment; filename=" + mSelectedDatabase);
            } else {
                output.println("Content-Length: " + bytes.length);
            }
            output.println();
            output.write(bytes);
            output.flush();
        } finally {
            try {
                if (null != output) {
                    output.close();
                }
                if (null != reader) {
                    reader.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void setCustomDatabaseFiles(HashMap<String, Pair<File, String>> customDatabaseFiles) {
        mCustomDatabaseFiles = customDatabaseFiles;
    }

    private void writeServerError(PrintStream output) {
        output.println("HTTP/1.0 500 Internal Server Error");
        output.flush();
    }

    private void openDatabase(String database) {
        closeDatabase();
        File databaseFile = mDatabaseFiles.get(database).first;
        String password = mDatabaseFiles.get(database).second;

        SQLiteDatabase.loadLibs(mContext);

        mDatabase = SQLiteDatabase.openOrCreateDatabase(databaseFile.getAbsolutePath(), password, null);
        isDbOpened = true;
    }

    private void closeDatabase() {
        if (mDatabase != null && mDatabase.isOpen()) {
            mDatabase.close();
        }
        mDatabase = null;
        isDbOpened = false;
    }

    private String getDBListResponse() {
        mDatabaseFiles = DatabaseFileProvider.getDatabaseFiles(mContext);
        if (mCustomDatabaseFiles != null) {
            mDatabaseFiles.putAll(mCustomDatabaseFiles);
        }
        Response response = new Response();
        if (mDatabaseFiles != null) {
            for (HashMap.Entry<String, Pair<File, String>> entry : mDatabaseFiles.entrySet()) {
                String[] dbEntry = {entry.getKey(), !entry.getValue().second.equals("") ? "true" : "false"};
                response.rows.add(dbEntry);
            }
        }
        response.rows.add(new String[]{Constants.APP_SHARED_PREFERENCES, "false"});
        response.isSuccessful = true;
        return mGson.toJson(response);
    }

    private String getAllDataFromTheTableResponse(String route) {

        String tableName = null;

        if (route.contains("?tableName=")) {
            tableName = route.substring(route.indexOf("=") + 1, route.length());
        }

        TableDataResponse response;
        String sql = "";
        if (isDbOpened) {
            if (tableName.equals("logs")) {
                sql = "SELECT * FROM " + tableName + " order by logId desc";
            } else {
                sql = "SELECT * FROM " + tableName;
            }

            response = DatabaseHelper.getTableData(mDatabase, sql, tableName);
        } else {
            response = PrefHelper.getAllPrefData(mContext, tableName);
        }

        return mGson.toJson(response);
    }

    private String executeQueryAndGetResponse(String route) {
        String query = null;
        String data = null;
        String first;
        try {
            if (route.contains("?query=")) {
                query = route.substring(route.indexOf("=") + 1, route.length());
            }
            try {
                query = URLDecoder.decode(query, "UTF-8");
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (query != null) {
                String[] statements = query.split(";");

                for (int i = 0; i < statements.length; i++) {

                    String aQuery = statements[i].trim();
                    first = aQuery.split(" ")[0].toLowerCase();
                    if (first.equals("select") || first.equals("pragma")) {
                        TableDataResponse response = DatabaseHelper.getTableData(mDatabase, aQuery, null);
                        data = mGson.toJson(response);
                        if (!response.isSuccessful) {
                            break;
                        }
                    } else {
                        TableDataResponse response = DatabaseHelper.exec(mDatabase, aQuery);
                        data = mGson.toJson(response);
                        if (!response.isSuccessful) {
                            break;
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (data == null) {
            Response response = new Response();
            response.isSuccessful = false;
            data = mGson.toJson(response);
        }

        return data;
    }

    private String getTableListResponse(String route) {
        String database = null;
        if (route.contains("?database=")) {
            database = route.substring(route.indexOf("=") + 1, route.length());
        }

        Response response;

        if (Constants.APP_SHARED_PREFERENCES.equals(database)) {
            response = PrefHelper.getAllPrefTableName(mContext);
            closeDatabase();
            mSelectedDatabase = Constants.APP_SHARED_PREFERENCES;
        } else {
            openDatabase(database);
            response = DatabaseHelper.getAllTableName(mDatabase);
            mSelectedDatabase = database;
        }
        return mGson.toJson(response);
    }


    private String addTableDataAndGetResponse(String route) {
        UpdateRowResponse response;
        try {
            Uri uri = Uri.parse(URLDecoder.decode(route, "UTF-8"));
            String tableName = uri.getQueryParameter("tableName");
            String updatedData = uri.getQueryParameter("addData");
            List<RowDataRequest> rowDataRequests = mGson.fromJson(updatedData, new TypeToken<List<RowDataRequest>>() {
            }.getType());
            if (Constants.APP_SHARED_PREFERENCES.equals(mSelectedDatabase)) {
                response = PrefHelper.addOrUpdateRow(mContext, tableName, rowDataRequests);
            } else {
                response = DatabaseHelper.addRow(mDatabase, tableName, rowDataRequests);
            }
            return mGson.toJson(response);
        } catch (Exception e) {
            e.printStackTrace();
            response = new UpdateRowResponse();
            response.isSuccessful = false;
            return mGson.toJson(response);
        }
    }

    private String updateTableDataAndGetResponse(String route) {
        UpdateRowResponse response;
        try {
            Uri uri = Uri.parse(URLDecoder.decode(route, "UTF-8"));
            String tableName = "configuration"; //uri.getQueryParameter("tableName");
            String updatedData = uri.getQueryParameter("updatedData");
            Log.i("updateData",updatedData);
            List<RowDataRequest> rowDataRequests = mGson.fromJson(updatedData, new TypeToken<List<RowDataRequest>>() {
            }.getType());
            Log.i("table",tableName);
            Log.i("pasoData",String.valueOf(rowDataRequests));
            if (Constants.APP_SHARED_PREFERENCES.equals(mSelectedDatabase)) {
                response = PrefHelper.addOrUpdateRow(mContext, tableName, rowDataRequests);
            } else {
                response = DatabaseHelper.updateRow(mDatabase, tableName, rowDataRequests);
            }
            Log.i("response",mGson.toJson(response));
            return mGson.toJson(response);
        } catch (Exception e) {
            e.printStackTrace();
            response = new UpdateRowResponse();
            response.isSuccessful = false;
            return mGson.toJson(response);
        }
    }


    private String deleteTableDataAndGetResponse(String route) {
        UpdateRowResponse response;
        try {
            Uri uri = Uri.parse(URLDecoder.decode(route, "UTF-8"));
            String tableName = uri.getQueryParameter("tableName");
            String updatedData = uri.getQueryParameter("deleteData");
            List<RowDataRequest> rowDataRequests = mGson.fromJson(updatedData, new TypeToken<List<RowDataRequest>>() {
            }.getType());
            if (Constants.APP_SHARED_PREFERENCES.equals(mSelectedDatabase)) {
                response = PrefHelper.deleteRow(mContext, tableName, rowDataRequests);
            } else {
                response = DatabaseHelper.deleteRow(mDatabase, tableName, rowDataRequests);
            }
            return mGson.toJson(response);
        } catch (Exception e) {
            e.printStackTrace();
            response = new UpdateRowResponse();
            response.isSuccessful = false;
            return mGson.toJson(response);
        }
    }

}
