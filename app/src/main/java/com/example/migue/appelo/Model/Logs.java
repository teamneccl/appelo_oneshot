package com.example.migue.appelo.Model;

/**
 * Created by macevedo on 10-01-2018.
 */

public class Logs {
    private long logId;
    private String logType, inputDesc,outputDesc,logDate;

    public Logs(long logId,String logType,String inputDesc,String outputDesc,String logDate) {
        this.logId = logId;
        this.logType = logType;
        this.inputDesc = inputDesc;
        this.outputDesc = outputDesc;
        this.logDate = logDate;
    }
    public Logs(){

    }

    public void setLogId(long logId) {
        this.logId = logId;
    }

    public void setLogDate(String logDate) {
        this.logDate = logDate;
    }

    public void setLogType(String logType) {
        this.logType = logType;
    }

    public void setInputDesc(String inputDesc) {
        this.inputDesc = inputDesc;
    }

    public void setOutputDesc(String outputDesc) {
        this.outputDesc = outputDesc;
    }

    public long getLogId() {

        return logId;
    }

    public String getLogDate() {
        return logDate;
    }

    public String getLogType() {
        return logType;
    }

    public String getInputDesc() {
        return inputDesc;
    }

    public String getOutputDesc() {
        return outputDesc;
    }

    public String toString(){
        return " logid: "+getLogId()+ "\n" +
                "logType: "+getLogType()  + "\n" +
                "inputDesc: "+getInputDesc()  + "\n" +
                "outputDesc: "+getOutputDesc() + "\n" +
                "logDate: "+getLogDate();
    }
}
