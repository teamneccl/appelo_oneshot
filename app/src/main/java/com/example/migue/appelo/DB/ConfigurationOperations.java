package com.example.migue.appelo.DB;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.migue.appelo.Model.Configuration;

import java.util.ArrayList;
import java.util.List;

public class ConfigurationOperations {
    public static final String LOGTAG = "EMP_MNGMNT_SYS";
    public Cursor cursor;

    SQLiteOpenHelper dbhandler;
    SQLiteDatabase database;

    private static final String[] allColumns = {
            ConfigurationDBHandler.COLUMN_ID,
            ConfigurationDBHandler.COLUMN_IPDESC,
            ConfigurationDBHandler.COLUMN_PORTDESC,
            ConfigurationDBHandler.COLUMN_TIPO,
            ConfigurationDBHandler.COLUMN_LOCAL,
            ConfigurationDBHandler.COLUMN_CONSULTOR,
            ConfigurationDBHandler.COLUMN_NOMBRELOCAL,
            ConfigurationDBHandler.COLUMN_URLWS,
            ConfigurationDBHandler.COLUMN_TPRECIOPANTALLA,
            ConfigurationDBHandler.COLUMN_SKIN,
            ConfigurationDBHandler.COLUMN_CONSULTASTOCK,
            ConfigurationDBHandler.COLUMN_STOCKMINIMO,
            ConfigurationDBHandler.COLUMN_STATUS,
            ConfigurationDBHandler.COLUMN_ABREVLOCAL,

            // publicidad
            ConfigurationDBHandler.COLUMN_MUESTRAPUBLICIDAD,
            ConfigurationDBHandler.COLUMN_CANTIMAGENES,
            ConfigurationDBHandler.COLUMN_TIEMPOIMAGEN,
            ConfigurationDBHandler.COLUMN_TIEMPOINACTIVIDAD,
            ConfigurationDBHandler.COLUMN_TIMESOCKET,
            ConfigurationDBHandler.COLUMN_TIMEOUTURLWS,
            ConfigurationDBHandler.COLUMN_LOGACTIVO,
            ConfigurationDBHandler.COLUMN_LOGNRODIAS,
            ConfigurationDBHandler.COLUMN_STOCKGRILLA,
            ConfigurationDBHandler.COLUMN_HOUR,
            ConfigurationDBHandler.COLUMN_MINUTE,
            ConfigurationDBHandler.COLUMN_VOZPRECIO,
            ConfigurationDBHandler.COLUMN_PASSWORD,

    };

    public ConfigurationOperations(Context context){
        dbhandler = new ConfigurationDBHandler(context);
    }

    public void open(){
        //Log.i(LOGTAG,"Database Opened");
        database = dbhandler.getWritableDatabase();


    }
    public void close(){
        //Log.i(LOGTAG, "Database Closed");
        dbhandler.close();

    }
    public Configuration addConfiguration(Configuration configuration){
        ContentValues values  = new ContentValues();
        values.put(ConfigurationDBHandler.COLUMN_IPDESC, configuration.getIPDesc());
        values.put(ConfigurationDBHandler.COLUMN_PORTDESC, configuration.getPORTDesc());
        values.put(ConfigurationDBHandler.COLUMN_TIPO, configuration.getTipo());

        values.put(ConfigurationDBHandler.COLUMN_LOCAL, configuration.getLocal());
        values.put(ConfigurationDBHandler.COLUMN_CONSULTOR, configuration.getConsultor());
        values.put(ConfigurationDBHandler.COLUMN_NOMBRELOCAL, configuration.getNombreLocal());
        values.put(ConfigurationDBHandler.COLUMN_URLWS, configuration.getUrlWs());
        values.put(ConfigurationDBHandler.COLUMN_TPRECIOPANTALLA, configuration.getTprecioPantalla());
        values.put(ConfigurationDBHandler.COLUMN_SKIN, configuration.getSkin());
        values.put(ConfigurationDBHandler.COLUMN_CONSULTASTOCK, configuration.getConsultaStock());
        values.put(ConfigurationDBHandler.COLUMN_STOCKMINIMO, configuration.getStockMinimo());
        values.put(ConfigurationDBHandler.COLUMN_STATUS, configuration.getStatus());
        values.put(ConfigurationDBHandler.COLUMN_ABREVLOCAL, configuration.getAbrevLocal());




        //publicidad
        values.put(ConfigurationDBHandler.COLUMN_MUESTRAPUBLICIDAD, configuration.getMuestraPublicidad());
        values.put(ConfigurationDBHandler.COLUMN_CANTIMAGENES, configuration.getCantImagenes());
        values.put(ConfigurationDBHandler.COLUMN_TIEMPOIMAGEN, configuration.getTiempoImagen());
        values.put(ConfigurationDBHandler.COLUMN_TIEMPOINACTIVIDAD, configuration.getTiempoInactividad());

        values.put(ConfigurationDBHandler.COLUMN_TIMESOCKET, configuration.getTimeSocket());
        values.put(ConfigurationDBHandler.COLUMN_TIMEOUTURLWS, configuration.getTimeWS());
        values.put(ConfigurationDBHandler.COLUMN_LOGACTIVO, configuration.getLogActivo());
        values.put(ConfigurationDBHandler.COLUMN_LOGNRODIAS, configuration.getLogNroDias());
        values.put(ConfigurationDBHandler.COLUMN_STOCKGRILLA, configuration.getStockGrilla());

        values.put(ConfigurationDBHandler.COLUMN_HOUR, configuration.getRebootHour());
        values.put(ConfigurationDBHandler.COLUMN_MINUTE, configuration.getRebootMinute());
        values.put(ConfigurationDBHandler.COLUMN_VOZPRECIO, configuration.getVozPrecio());
        values.put(ConfigurationDBHandler.COLUMN_PASSWORD, configuration.getContrasenaAdmin());

        long insertid = database.insert(ConfigurationDBHandler.TABLE_CONFIGURATION,null,values);
        configuration.setEmpId(insertid);
        return configuration;

    }

    // Getting single Configuration
    public Configuration getConfiguration(long id) {
        Configuration e = new Configuration();
        cursor = database.query(ConfigurationDBHandler.TABLE_CONFIGURATION,allColumns,ConfigurationDBHandler.COLUMN_ID + "=?",new String[]{String.valueOf(id)},null,null, null, null);
        try {
            if (cursor.getCount() > 0)
                cursor.moveToFirst();
            else
                return null;
            if(cursor.getCount() > 0) {
                e = new Configuration(Long.parseLong(cursor.getString(0)), cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4),Long.parseLong(personalCast(cursor.getString(5))),cursor.getString(6),cursor.getString(7),Integer.parseInt(personalCast(cursor.getString(8))),Integer.parseInt(personalCast(cursor.getString(9))),Integer.parseInt(personalCast(cursor.getString(10)) ),Integer.parseInt(personalCast(cursor.getString(11))),Integer.parseInt(personalCast(cursor.getString(12))),cursor.getString(13),Integer.parseInt(personalCast(cursor.getString(14))),Integer.parseInt(personalCast(cursor.getString(15))),Integer.parseInt(personalCast(cursor.getString(16))),Integer.parseInt(personalCast(cursor.getString(17))),Integer.parseInt(personalCast(cursor.getString(18))),Integer.parseInt(personalCast(cursor.getString(19))),Integer.parseInt(personalCast(cursor.getString(20))),Integer.parseInt(personalCast(cursor.getString(21))),Integer.parseInt(personalCast(cursor.getString(22))),Integer.parseInt(personalCast(cursor.getString(23))),Integer.parseInt(personalCast(cursor.getString(24))),Integer.parseInt(personalCast(cursor.getString(25))),Integer.parseInt(personalCast(cursor.getString(26))));
            }
        } finally {
            cursor.close();
            database.close();
        }

        // return Configuration

        return e;
    }

    // Getting single Configuration
    public Configuration getConfigurationByStatus(Integer status) {
        Configuration e = new Configuration();
        cursor = database.query(ConfigurationDBHandler.TABLE_CONFIGURATION,allColumns,ConfigurationDBHandler.COLUMN_STATUS + "=?",new String[]{String.valueOf(status)},null,null, null, null);
        try {
            if (cursor.getCount() > 0)
                cursor.moveToFirst();
            else
                return null;
            if(cursor.getCount() > 0) {
                e = new Configuration(Long.parseLong(cursor.getString(0)), cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4),Long.parseLong(personalCast(cursor.getString(5))),cursor.getString(6),cursor.getString(7),Integer.parseInt(personalCast(cursor.getString(8))),Integer.parseInt(personalCast(cursor.getString(9))),Integer.parseInt(personalCast(cursor.getString(10)) ),Integer.parseInt(personalCast(cursor.getString(11))),Integer.parseInt(personalCast(cursor.getString(12))),cursor.getString(13),Integer.parseInt(personalCast(cursor.getString(14))),Integer.parseInt(personalCast(cursor.getString(15))),Integer.parseInt(personalCast(cursor.getString(16))),Integer.parseInt(personalCast(cursor.getString(17))),Integer.parseInt(personalCast(cursor.getString(18))),Integer.parseInt(personalCast(cursor.getString(19))),Integer.parseInt(personalCast(cursor.getString(20))),Integer.parseInt(personalCast(cursor.getString(21))),Integer.parseInt(personalCast(cursor.getString(22))),Integer.parseInt(personalCast(cursor.getString(23))),Integer.parseInt(personalCast(cursor.getString(24))),Integer.parseInt(personalCast(cursor.getString(25))),Integer.parseInt(personalCast(cursor.getString(26))));
            }
        } finally {
            cursor.close();
            database.close();
        }


        // return Configuration
        return e;
    }

    private String personalCast(String numero){
        String number = "0";
        Integer revision = 0;
        //Log.i("numero",numero);
        try
        {       revision = Integer.valueOf(numero);
                return numero;
        }
        catch (NumberFormatException e)
        {
            Log.i("excep","error" + String.valueOf(e));
           return "0";
        }

    }

    // Getting single Configuration
    public Configuration getConfiguration(String tipo) {
        Configuration e = new Configuration();

        cursor = database.query(ConfigurationDBHandler.TABLE_CONFIGURATION,allColumns,ConfigurationDBHandler.COLUMN_TIPO + "=?",new String[]{String.valueOf(tipo)},null,null, null, null);
try {
    if (cursor.getCount() > 0)
        cursor.moveToFirst();
    else
        return null;
    if(cursor.getCount() > 0) {
        e = new Configuration(Long.parseLong(cursor.getString(0)), cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4),Long.parseLong(personalCast(cursor.getString(5))),cursor.getString(6),cursor.getString(7),Integer.parseInt(personalCast(cursor.getString(8))),Integer.parseInt(personalCast(cursor.getString(9))),Integer.parseInt(personalCast(cursor.getString(10)) ),Integer.parseInt(personalCast(cursor.getString(11))),Integer.parseInt(personalCast(cursor.getString(12))),cursor.getString(13),Integer.parseInt(personalCast(cursor.getString(14))),Integer.parseInt(personalCast(cursor.getString(15))),Integer.parseInt(personalCast(cursor.getString(16))),Integer.parseInt(personalCast(cursor.getString(17))),Integer.parseInt(personalCast(cursor.getString(18))),Integer.parseInt(personalCast(cursor.getString(19))),Integer.parseInt(personalCast(cursor.getString(20))),Integer.parseInt(personalCast(cursor.getString(21))),Integer.parseInt(personalCast(cursor.getString(22))),Integer.parseInt(personalCast(cursor.getString(23))),Integer.parseInt(personalCast(cursor.getString(24))),Integer.parseInt(personalCast(cursor.getString(25))),Integer.parseInt(personalCast(cursor.getString(26))));
        // Log.i("socketcursor",cursor.getString(19));
    }
} finally {
    cursor.close();
}

        // return Configuration

        return e;
    }

    public List<Configuration> getAllConfigurations() {

        cursor = database.query(ConfigurationDBHandler.TABLE_CONFIGURATION,allColumns,null,null,null, null, null);

        List<Configuration> configurationList = new ArrayList<>();
        if(cursor.getCount() > 0){
            while(cursor.moveToNext()){
                Configuration configuration = new Configuration();
                configuration.setEmpId(cursor.getLong(cursor.getColumnIndex(ConfigurationDBHandler.COLUMN_ID)));
                configuration.setIPDesc(cursor.getString(cursor.getColumnIndex(ConfigurationDBHandler.COLUMN_IPDESC)));
                configuration.setPORTDesc(cursor.getString(cursor.getColumnIndex(ConfigurationDBHandler.COLUMN_PORTDESC)));
                configuration.setTipo(cursor.getString(cursor.getColumnIndex(ConfigurationDBHandler.COLUMN_TIPO)));

                configuration.setLocal(cursor.getString(cursor.getColumnIndex(ConfigurationDBHandler.COLUMN_LOCAL)));
                configuration.setConsultor(cursor.getLong(cursor.getColumnIndex(ConfigurationDBHandler.COLUMN_CONSULTOR)));
                configuration.setNombreLocal(cursor.getString(cursor.getColumnIndex(ConfigurationDBHandler.COLUMN_NOMBRELOCAL)));
                configuration.setUrlWs(cursor.getString(cursor.getColumnIndex(ConfigurationDBHandler.COLUMN_URLWS)));
                configuration.setTprecioPantalla(cursor.getInt(cursor.getColumnIndex(ConfigurationDBHandler.COLUMN_TPRECIOPANTALLA)));
                configuration.setSkin(cursor.getInt(cursor.getColumnIndex(ConfigurationDBHandler.COLUMN_SKIN)));
                configuration.setConsultaStock(cursor.getInt(cursor.getColumnIndex(ConfigurationDBHandler.COLUMN_CONSULTASTOCK)));
                configuration.setStockMinimo(cursor.getInt(cursor.getColumnIndex(ConfigurationDBHandler.COLUMN_STOCKMINIMO)));
                configuration.setStatus(cursor.getInt(cursor.getColumnIndex(ConfigurationDBHandler.COLUMN_STATUS)));
                configuration.setAbrevLocal(cursor.getString(cursor.getColumnIndex(ConfigurationDBHandler.COLUMN_ABREVLOCAL)));



                //publicidad
                configuration.setMuestraPublicidad(cursor.getInt(cursor.getColumnIndex(ConfigurationDBHandler.COLUMN_MUESTRAPUBLICIDAD)));
                configuration.setCantImagenes(cursor.getInt(cursor.getColumnIndex(ConfigurationDBHandler.COLUMN_CANTIMAGENES)));
                configuration.setTiempoImagen(cursor.getInt(cursor.getColumnIndex(ConfigurationDBHandler.COLUMN_TIEMPOIMAGEN)));
                configuration.setTiempoInactividad(cursor.getInt(cursor.getColumnIndex(ConfigurationDBHandler.COLUMN_TIEMPOINACTIVIDAD)));

                configuration.setTimeSocket(cursor.getInt(cursor.getColumnIndex(ConfigurationDBHandler.COLUMN_TIMESOCKET)));
                configuration.setTimeWS(cursor.getInt(cursor.getColumnIndex(ConfigurationDBHandler.COLUMN_TIMEOUTURLWS)));
                configuration.setLogActivo(cursor.getInt(cursor.getColumnIndex(ConfigurationDBHandler.COLUMN_LOGACTIVO)));
                configuration.setLogNroDias(cursor.getInt(cursor.getColumnIndex(ConfigurationDBHandler.COLUMN_LOGNRODIAS)));

                configuration.setStockGrilla(cursor.getInt(cursor.getColumnIndex(ConfigurationDBHandler.COLUMN_STOCKGRILLA)));

                configuration.setRebootHour(cursor.getInt(cursor.getColumnIndex(ConfigurationDBHandler.COLUMN_HOUR)));
                configuration.setRebootMinute(cursor.getInt(cursor.getColumnIndex(ConfigurationDBHandler.COLUMN_MINUTE)));

                configuration.setVozPrecio(cursor.getInt(cursor.getColumnIndex(ConfigurationDBHandler.COLUMN_VOZPRECIO)));

                configuration.setContrasenaAdmin(cursor.getInt(cursor.getColumnIndex(ConfigurationDBHandler.COLUMN_PASSWORD)));

                configurationList.add(configuration);
            }
        }
        cursor.close();
        // return All Configuration
        return configurationList;
    }




    // Updating Configuration
    public int updateConfiguration(Configuration configuration) {

        ContentValues values = new ContentValues();
        values.put(ConfigurationDBHandler.COLUMN_IPDESC, configuration.getIPDesc());
        values.put(ConfigurationDBHandler.COLUMN_PORTDESC, configuration.getPORTDesc());
        values.put(ConfigurationDBHandler.COLUMN_TIPO, configuration.getTipo());
        values.put(ConfigurationDBHandler.COLUMN_LOCAL, configuration.getLocal());
        values.put(ConfigurationDBHandler.COLUMN_CONSULTOR, configuration.getConsultor());
        values.put(ConfigurationDBHandler.COLUMN_NOMBRELOCAL, configuration.getNombreLocal());
        values.put(ConfigurationDBHandler.COLUMN_URLWS, configuration.getUrlWs());
        values.put(ConfigurationDBHandler.COLUMN_TPRECIOPANTALLA, configuration.getTprecioPantalla());
        values.put(ConfigurationDBHandler.COLUMN_SKIN, configuration.getSkin());
        values.put(ConfigurationDBHandler.COLUMN_CONSULTASTOCK, configuration.getConsultaStock());
        values.put(ConfigurationDBHandler.COLUMN_STOCKMINIMO, configuration.getStockMinimo());
        values.put(ConfigurationDBHandler.COLUMN_STATUS, configuration.getStatus());
        values.put(ConfigurationDBHandler.COLUMN_ABREVLOCAL, configuration.getAbrevLocal());



        //publicidad
        values.put(ConfigurationDBHandler.COLUMN_MUESTRAPUBLICIDAD, configuration.getMuestraPublicidad());
        values.put(ConfigurationDBHandler.COLUMN_CANTIMAGENES, configuration.getCantImagenes());
        values.put(ConfigurationDBHandler.COLUMN_TIEMPOIMAGEN, configuration.getTiempoImagen());
        values.put(ConfigurationDBHandler.COLUMN_TIEMPOINACTIVIDAD, configuration.getTiempoInactividad());

        values.put(ConfigurationDBHandler.COLUMN_TIMESOCKET, configuration.getTimeSocket());
        values.put(ConfigurationDBHandler.COLUMN_TIMEOUTURLWS, configuration.getTimeWS());
        values.put(ConfigurationDBHandler.COLUMN_LOGACTIVO, configuration.getLogActivo());
        values.put(ConfigurationDBHandler.COLUMN_LOGNRODIAS, configuration.getLogNroDias());
        values.put(ConfigurationDBHandler.COLUMN_STOCKGRILLA, configuration.getStockGrilla());

        values.put(ConfigurationDBHandler.COLUMN_HOUR, configuration.getRebootHour());
        values.put(ConfigurationDBHandler.COLUMN_MINUTE, configuration.getRebootMinute());
        values.put(ConfigurationDBHandler.COLUMN_VOZPRECIO, configuration.getVozPrecio());

        values.put(ConfigurationDBHandler.COLUMN_PASSWORD, configuration.getContrasenaAdmin());

        // updating row
        return database.update(ConfigurationDBHandler.TABLE_CONFIGURATION, values,
                ConfigurationDBHandler.COLUMN_ID + "=?",new String[] { String.valueOf(configuration.getEmpId())});
    }

    // Deleting Configuration
    public void removeConfiguration(Configuration configuration) {

        database.delete(ConfigurationDBHandler.TABLE_CONFIGURATION, ConfigurationDBHandler.COLUMN_ID + "=" + configuration.getEmpId(), null);
    }



}
